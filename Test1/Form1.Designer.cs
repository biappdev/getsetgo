﻿namespace Test1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.applySettingsBtn = new System.Windows.Forms.Button();
            this.lblHelloWorld = new System.Windows.Forms.Label();
            this.browseVdBtn = new System.Windows.Forms.Button();
            this.vdLabel = new System.Windows.Forms.Label();
            this.labeldone = new System.Windows.Forms.Label();
            this.button_intePub = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // applySettingsBtn
            // 
            this.applySettingsBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.applySettingsBtn.Location = new System.Drawing.Point(835, 526);
            this.applySettingsBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.applySettingsBtn.Name = "applySettingsBtn";
            this.applySettingsBtn.Size = new System.Drawing.Size(91, 36);
            this.applySettingsBtn.TabIndex = 0;
            this.applySettingsBtn.Text = "Validate";
            this.applySettingsBtn.UseVisualStyleBackColor = true;
            this.applySettingsBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblHelloWorld
            // 
            this.lblHelloWorld.AutoSize = true;
            this.lblHelloWorld.Location = new System.Drawing.Point(93, 55);
            this.lblHelloWorld.Name = "lblHelloWorld";
            this.lblHelloWorld.Size = new System.Drawing.Size(0, 17);
            this.lblHelloWorld.TabIndex = 1;
            // 
            // browseVdBtn
            // 
            this.browseVdBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.browseVdBtn.Location = new System.Drawing.Point(21, 22);
            this.browseVdBtn.Margin = new System.Windows.Forms.Padding(4);
            this.browseVdBtn.Name = "browseVdBtn";
            this.browseVdBtn.Size = new System.Drawing.Size(118, 49);
            this.browseVdBtn.TabIndex = 2;
            this.browseVdBtn.Text = "Browse Virtual Directory";
            this.browseVdBtn.UseVisualStyleBackColor = true;
            this.browseVdBtn.Click += new System.EventHandler(this.browseVdBtn_Click);
            // 
            // vdLabel
            // 
            this.vdLabel.AutoSize = true;
            this.vdLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.vdLabel.Location = new System.Drawing.Point(182, 54);
            this.vdLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.vdLabel.Name = "vdLabel";
            this.vdLabel.Size = new System.Drawing.Size(0, 17);
            this.vdLabel.TabIndex = 3;
            // 
            // labeldone
            // 
            this.labeldone.AutoSize = true;
            this.labeldone.Location = new System.Drawing.Point(428, 537);
            this.labeldone.Name = "labeldone";
            this.labeldone.Size = new System.Drawing.Size(0, 17);
            this.labeldone.TabIndex = 5;
            // 
            // button_intePub
            // 
            this.button_intePub.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_intePub.Location = new System.Drawing.Point(797, 22);
            this.button_intePub.Margin = new System.Windows.Forms.Padding(4);
            this.button_intePub.Name = "button_intePub";
            this.button_intePub.Size = new System.Drawing.Size(129, 49);
            this.button_intePub.TabIndex = 6;
            this.button_intePub.Text = "Browse InetPub";
            this.button_intePub.UseVisualStyleBackColor = true;
            this.button_intePub.Click += new System.EventHandler(this.btn_intePub);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(352, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 17);
            this.label1.TabIndex = 7;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.Location = new System.Drawing.Point(146, 22);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(80, 21);
            this.radioButton1.TabIndex = 8;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "MobVD";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton2.Location = new System.Drawing.Point(227, 22);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(82, 21);
            this.radioButton2.TabIndex = 9;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "WebVD";
            this.radioButton2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(956, 623);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_intePub);
            this.Controls.Add(this.labeldone);
            this.Controls.Add(this.vdLabel);
            this.Controls.Add(this.browseVdBtn);
            this.Controls.Add(this.lblHelloWorld);
            this.Controls.Add(this.applySettingsBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MinimumSize = new System.Drawing.Size(887, 564);
            this.Name = "Form1";
            this.Text = "SecurityApp";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button applySettingsBtn;
        private System.Windows.Forms.Label lblHelloWorld;
        private System.Windows.Forms.Button browseVdBtn;
        private System.Windows.Forms.Label vdLabel;
        private System.Windows.Forms.CheckedListBox settingsListBox;
        private System.Windows.Forms.Label labeldone;
        private System.Windows.Forms.Button button_intePub;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
    }
}

