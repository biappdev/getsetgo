﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using System.Windows;

namespace Test1
{
    public partial class Form1 : Form
    {
        public String filepath;
        public String inetPath;
        public String propFile;
        public int signal = 0;
        ListView listView1 = new ListView();
        int[] arr = new int[21];
        int[] arrInet = new int[13]; 
        ListViewItem item;
        ListViewRow[] arrlistView;
        //  List<string[]> tagName = new List<string[]>();
        public event System.ComponentModel.CancelEventHandler Closing;

  

        List<ListViewItem> myItems = new List<ListViewItem>();

        public Form1()
        {
            InitializeComponent();

            labeldone.Text += "inside function";

           // this.FormClosing += Form1_FormClosing1;

         //   this.FormClosing += Form1_FormClosing;
            //   inetTextPath.GotFocus += RemoveText ;
            //  inetTextPath.LostFocus += AddText;

            //   tagName.Add(new string[] { "item 1", "item 2", "item 3" });
            //  myItems.AddRange(listView1.Items.OfType<ListViewItem>());

            //ImageList imageListSmall = new ImageList();
            // ImageList imageListLarge = new ImageList(); //for big size image like 2x in iOS 

            //Assign the ImageList objects to the ListView.
            // listView1.LargeImageList = imageListLarge;


            // Initialize the ImageList ob  jects with bitmaps.
            // imageListSmall.Images.Add(Bitmap.FromFile("C:\\Users\\ss19405\\Documents\\CodeBase\\Test1\\Test1\\Properties\\check.bmp"));
            //imageListSmall.Images.Add(Bitmap.FromFile("C:\\Users\\ss19405\\Documents\\CodeBase\\Test1\\Test1\\Properties\\button_minus_red.bmp"));

            //            inetTextPath.Text = "Enter InetPub Config File Path";

            string[] serianlNo = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18","19","20","21" };
            //string[] tagName = { "Sample 1", "Sample 2", "Sample 3" };

            String[] settingName =
            {   //urlscan
                "Request Filtering Rule",
                "Disable OPTION Method",
                "Enabling Secure Flag/HTTPOnly cookie",
                "Hide Programming Language/Version",
                "Hiding Server/Powered By",
                "Adding HSTS Header",
                "Timeout setting",
                "Custom Error Messages",
                "hidingAspVersion",
                "hidingServerresponse",
                "Include time stamp in error message",
                "Session information is included on the URL",
                "Allow users to change expired password",
                "Allow automatic login if session is lost",
                "Users to log including their UserID and Pass in URL",
                "Users the ‘Web administrator’ privilege",
                "Prevent clickjacking by adding a frame - breaking script to pages",
                "Prevent clickjacking by adding an X - Frame - Options: SAMEORIGIN header to page response",
                "loginFirst",
                "serverSorting",
                "projectSorting"
            };

            string[] adressVD = { "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA" };
            string[] adressInet = { "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA" , "NA", "NA", "NA" };

            //   imageListSmall.Images.Add(Image.FromFile("C:\\Users\\ss19405\\Documents\\CodeBase\\Test1\\Test1\\Properties\\penguine_mac_archigraphs.bmp"));
            // imageListSmall.Images.Add(Bitmap.FromFile("C:\\Users\\ss19405\\Documents\\CodeBase\\Test1\\Test1\\Properties\\penguine_mac_archigraphs.bmp"));
            //imageListSmall.Images.Add(Bitmap.FromFile("C:\\Users\\ss19405\\Documents\\CodeBase\\Test1\\Test1\\Properties\\penguine_mac_archigraphs.bmp"));


            //Console.WriteLine("tagName"+adress.Length+image.Length);
            for (int i = 0; i < settingName.Length; i++)
            {
                listView1.Items.Add(new ListViewItem(new[] { serianlNo[i], settingName[i], adressVD[i] , adressInet[i] }));

            }

            ListViewRow objListView = new ListViewRow();
            arrlistView = new ListViewRow[settingName.Length];

            for (int i = 0; i < settingName.Length; i++)
            {
                ListViewRow obj = new ListViewRow();
                obj.settingName = settingName[i];
                obj.vdCheck = "NA";
                obj.inetPub = "NA";
                arrlistView[i] = obj;
            }

            /*
           //for printing //get
            for (int i = 0; i < settingName.Length; i++)
            {
              
                Console.WriteLine(arr[i].settingName);
                Console.WriteLine(arr[i].vdCheck);
                Console.WriteLine(arr[i].inetPub);

            }
            */

                // set ho sakta hai
                // arr[4].vdCheck = "dd";




                //netTextPath.GotFocus += GotFocus.Eventhandle()
                // inetTextPath.GotFocus += EventHandle(RemoveText);
                // inetTextPath.LostFocus += LostFocus.EventHandle(AddText);

                CreateMyListView(listView1);
        }

        private void Form1_FormClosing1(object sender, FormClosingEventArgs e)
        {

            Console.WriteLine("Main class");

            if (e.CloseReason == CloseReason.UserClosing)
            {
                Console.WriteLine("inside cancel event");
                Main main1 = new Main();

                this.Hide();

                main1.Closed += (s, args) => this.Close();
                //  form2.Show();
                // Show the settings form
                main1.Show();
            }
        }

        /*
        private void InetTextPath_LostFocus(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void InetTextPath_GotFocus(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public void RemoveText(object sender, EventArgs e)
        {
            if (inetTextPath.Text == "Enter InetPub Config File Path")
            {
                inetTextPath.Text = "";
            }
        }

        public void AddText(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(inetTextPath.Text))
                inetTextPath.Text = "Enter InetPub Config File Path";
        }

            */

        private void button1_Click(object sender, EventArgs e)
        {
            for(int i =0; i < arr.Length; i++)
            {
                listView1.Items[i].SubItems[0].ForeColor = Color.Black;
            }
           

            labeldone.Text = "";
            //   myTxtbx.GotFocus += GotFocus.EventHandle(RemoveText);
            //  myTxtbx.LostFocus += LostFocus.EventHandle(AddText);
            //  CreateMyListView(listView1);
            //validate button
            //     settingsListBox.SelectionMode = SelectionMode.One;

            /*
            foreach (int i in settingsListBox.CheckedIndices)
            {
                settingsListBox.SetItemCheckState(i, CheckState.Unchecked);
            }
            */
            
            XmlDataDocument doc1 = new XmlDataDocument();

            //   inetPath = inetTextPath.Text;
           // cleanPath(filepath);


            Console.WriteLine("filepath123" + filepath);
            Console.WriteLine("inetpub123" + inetPath);
            if (filepath != String.Empty && inetPath != String.Empty)
            {
                try {
                    XmlDataDocument doc = new XmlDataDocument();

                    String xmlData = File.ReadAllText(filepath);



                    Console.WriteLine("inetPath" + inetPath);



                    String xmlDataInet = File.ReadAllText(inetPath);

                    try
                    {
                        doc.LoadXml(xmlData);
                        doc1.LoadXml(xmlDataInet);
                        Console.WriteLine(filepath);
                        Console.WriteLine("inet" + inetPath);

                    }
                    catch (Exception exp)
                    {
                        MessageBox.Show("Web.Config file seems corrupt.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                    
             

                    try
                    { 
                        parseXML(doc, doc1);
                        //labeldone.Text = "xml parsed";
                    }
                    catch (Exception exp)
                    {
                        MessageBox.Show(exp.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        labeldone.Text = exp.Message.ToString()+"xml parsing";
                    }


                    if (radioButton1.Checked != true)
                    {
                        try
                        {
                            properties1();
                            labeldone.Text = "Done";

                        }
                        catch (Exception exp)
                        {
                            labeldone.Text = exp.Message.ToString() + "properties file error";
                        }
                    }
   

                 
                     
                                      
                }
                
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            }else
            {
                MessageBox.Show("Please enter correct path for Web.Config Files", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            /*
                        try
                        {
                            if (filepath != "")
                            {
                                try
                                {
                                    if (filepath.Contains("inetpub"))
                                    {
                                        signal = 0;
                                        parseXML(doc, signal, doc1);
                                    }
                                    else
                                    {

                                        signal = 1;
                                        

                                    }

                                }
                                catch
                                {
                                    MessageBox.Show("FilePath Empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                        }
                        catch
                        {
                            MessageBox.Show("Error", "Please select the file again", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                        */

            // vdLabel.Text = "Done";


            //   settingsListBox.SelectionMode = SelectionMode.None;

        }

        private void browseVdBtn_Click(object sender, EventArgs e)
        {
            /*
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                Title = "Browse config file",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "config",
                Filter = "config files (*.config)|*.config",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };
           

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                vdLabel.Text = openFileDialog1.FileName;
                filepath = System.IO.Path.GetFullPath(openFileDialog1.FileName);
            }
            */

           // string folderPath = "";
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                filepath = folderBrowserDialog1.SelectedPath;
                
            }


            if (filepath != null)
            {
                filepath = Path.Combine(filepath, "web.config");
                vdLabel.Text = filepath;
            }
            else
            {
                MessageBox.Show("Please select filepath", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

           
            //Console.WriteLine("filepath" + filepath);
        }

        private void btn_intePub(object sender, EventArgs e)
        {
            /*
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                Title = "Browse config file",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "config",
                Filter = "config files (*.config)|*.config",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };
           

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                vdLabel.Text = openFileDialog1.FileName;
                filepath = System.IO.Path.GetFullPath(openFileDialog1.FileName);
            }
            */

            // string folderPath = "";
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                inetPath = folderBrowserDialog1.SelectedPath;

            }

            if (inetPath != null)
            {
                inetPath = Path.Combine(inetPath, "wwwroot");
                inetPath = Path.Combine(inetPath, "web.config");
                label1.Text = inetPath;
            }
            else
            {
                MessageBox.Show("Please select filepath", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }          
            //Console.WriteLine("filepath" + filepath);
        }



        private void parseXML(XmlDataDocument doc , XmlDataDocument doc1)
        {

            labeldone.Text = "parseXML";
            Dictionary<string, string> data = new Dictionary<string, string>();
            Console.WriteLine(signal);
          
            Console.WriteLine(filepath);



           Console.WriteLine("proFile"+ propFile);

            if (radioButton1.Checked != true)
            {


                Console.WriteLine("fileName " + Path.GetFileNameWithoutExtension(filepath));
                propFile = Path.GetDirectoryName(filepath);

                propFile = Path.Combine(propFile, "WEB-INF");
                propFile = Path.Combine(propFile, "xml");
                propFile = Path.Combine(propFile, "sys_defaults.properties");
                cleanPath(propFile);

                try
                {
                    foreach (var row in File.ReadAllLines(propFile))
                        data.Add(row.Split('=')[0], string.Join("=", row.Split('=').Skip(1).ToArray()));
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    labeldone.Text = e.Message.ToString() + "propfile";
                }
            }
        

         



            //yaya signal wala if case tha == wala



            //Display the document element.
            //  Console.WriteLine(doc.DocumentElement.OuterXml);

            //     XmlElement root = doc.DocumentElement; // root node
            //     XmlNodeList nodes = doc.DocumentElement.ChildNodes;


            //   XmlNodeList elemList = doc.GetElementsByTagName("requestFiltering");
            //Console.WriteLine(elemList);
            /*
            ArrayList checkListIndex = new ArrayList();
            checkListIndex.Add(0);
            checkListIndex.Add(1);
            checkListIndex.Add(2);
            checkListIndex.Add(3);
            checkListIndex.Add(4);
            checkListIndex.Add(5);
            checkListIndex.Add(6);
            checkListIndex.Add(7);
            checkListIndex.Add(8);
            checkListIndex.Add(9);
            checkListIndex.Add(10);
            checkListIndex.Add(11);
            checkListIndex.Add(12);
            checkListIndex.Add(13);
            checkListIndex.Add(14);
            checkListIndex.Add(15);
            checkListIndex.Add(16);
            checkListIndex.Add(17);
            */
            //  checkListIndex.Add("18");
            // checkListIndex.Add("19");

            /*
            if (signal == 1)
            {             
                checkListIndex.Add("11");
                checkListIndex.Add("12");
                checkListIndex.Add("13");
                checkListIndex.Add("14");
                checkListIndex.Add("15");
                checkListIndex.Add("16");
                checkListIndex.Add("17");
                checkListIndex.Add("18");
                checkListIndex.Add("19");

            }else
            {
                listView1.Items[9].SubItems[0].ForeColor = Color.Black;
                listView1.Items[10].SubItems[0].ForeColor = Color.Black;
                listView1.Items[11].SubItems[0].ForeColor = Color.Black;
                listView1.Items[12].SubItems[0].ForeColor = Color.Black;
                listView1.Items[13].SubItems[0].ForeColor = Color.Black;
                listView1.Items[14].SubItems[0].ForeColor = Color.Black;
                listView1.Items[15].SubItems[0].ForeColor = Color.Black;
                listView1.Items[16].SubItems[0].ForeColor = Color.Black;
             //   listView1.Items[17].SubItems[0].ForeColor = Color.Black;

            }
           
            */

            /*
            foreach (object indexChecked in settingsListBox.CheckedIndices)
            {

                Console.WriteLine("Index#: " + indexChecked.ToString() + ", is checked. Checked state is:" + settingsListBox.GetItemCheckState((int)indexChecked).ToString() + ".");
                checkListIndex.Add(indexChecked.ToString());

            }
            */

            //dynamic yaha ban salta hai
            Console.WriteLine("start checking");

            
            int length = 0;
            if (radioButton1.Checked == true) {
                length = 10;
            }
            else
            {
                length = arr.Length;
            }

            Console.WriteLine("radioFlag" + radioButton1.Checked.ToString());

            try
            {
                {
                    for (int s = 0; s < length; s++)
                
                    //Console.WriteLine(s);

                    switch (s)
                    {

                        case 0:

                            arrlistView[s].vdCheck = requestFilteringRules(doc).ToString();
                            arrlistView[s].inetPub = requestFilteringRules(doc1).ToString();
                            //  arr[s] = requestFilteringRules(doc);
                            //  arrInet[s] = requestFilteringRules(doc1);
                            Console.WriteLine("requestFilteringRules");

                            break;
                        case 1:
                            arrlistView[s].vdCheck = requestFilteringOption(doc).ToString();
                            arrlistView[s].inetPub = requestFilteringOption(doc1).ToString();
                            Console.WriteLine("requestFilteringOption");
                            //    arr[s] = requestFilteringOption(doc);
                            //     arrInet[s] =  requestFilteringOption(doc1);

                            break;
                        case 2:
                            arrlistView[s].vdCheck = httpCookie(doc).ToString();
                            arrlistView[s].inetPub = httpCookie(doc1).ToString();
                            Console.WriteLine("httpCookie");
                            //    arr[s] = httpCookie(doc);
                            //     arrInet[s]= httpCookie(doc1);
                            break;
                        case 3:
                            arrlistView[s].vdCheck = enableVersionHeader(doc).ToString();
                            arrlistView[s].inetPub = enableVersionHeader(doc1).ToString();
                            Console.WriteLine("enableVersionHeader");
                            //   arr[s] = enableVersionHeader(doc);
                            //    arrInet[s] = enableVersionHeader(doc1);
                            break;
                        case 4:
                            arrlistView[s].vdCheck = hidingServer(doc).ToString();
                            arrlistView[s].inetPub = hidingServer(doc1).ToString();
                            Console.WriteLine("hidingServer");

                            //     arr[s] = hidingServer(doc);
                            //      arrInet[s] = hidingServer(doc1);
                            break;
                        case 5:

                            arrlistView[s].vdCheck = ( hstsheader(doc) | hstsheader1(doc) ).ToString();
                            arrlistView[s].inetPub = hstsheader(doc1).ToString();
                            Console.WriteLine("hstsheader");
                            //      arr[s] = hstsheader(doc);
                            //    arrInet[s] =  hstsheader(doc1);
                            break;
                        case 6:

                            arrlistView[s].vdCheck = timeOut(doc).ToString();
                            arrlistView[s].inetPub = timeOut(doc1).ToString();
                            Console.WriteLine("timeOut");
                            //     arr[s] = timeOut(doc);
                            //     arrInet[s] = timeOut(doc1);
                            break;
                        case 7:
                            arrlistView[s].vdCheck = customError(doc).ToString();
                            arrlistView[s].inetPub = customError(doc1).ToString();
                            Console.WriteLine("customError");
                            //     arr[s] = customError(doc);
                            //     arrInet[s] = customError(doc1);
                            break;
                        case 8:
                            arrlistView[s].vdCheck = hidingAspVersion(doc).ToString();
                            arrlistView[s].inetPub = hidingAspVersion(doc1).ToString();
                            Console.WriteLine("hidingAspVersion");
                            //     arr[s] = hidingAspVersion(doc);
                            //     arrInet[s] = hidingAspVersion(doc1);
                            break;
                        case 9:
                            arrlistView[s].vdCheck = hidingServerResponse(doc).ToString();
                            arrlistView[s].inetPub = hidingServerResponse(doc1).ToString();
                            Console.WriteLine("hidingServerResponse");
                            //    arr[s] = hidingServerResponse(doc); 
                            //     arrInet[s] = hidingServerResponse(doc1);
                            break;
                        case 10:
                            arrlistView[s].vdCheck = includeTimeStamp(data).ToString();
                            //  arr[s] = includeTimeStamp(data);
                            break;
                        case 11:
                            arrlistView[s].vdCheck = includeSessionUrl(data).ToString();
                            //   arr[s] = includeSessionUrl(data);
                            break;
                        case 12:
                            arrlistView[s].vdCheck = changePwd(data).ToString();
                            //   arr[s] = changePwd(data);
                            break;
                        case 13:
                            arrlistView[s].vdCheck = allowSeamlessLogin(data).ToString();
                            //    arr[s] = allowSeamlessLogin(data);
                            break;
                        case 14:
                            arrlistView[s].vdCheck = allowLoginByUrl(data).ToString();
                            //   arr[s] = allowLoginByUrl(data);
                            break;
                        case 15:
                            arrlistView[s].vdCheck = allowAdminApplyAllProjects(data).ToString();
                            //    arr[s] = allowAdminApplyAllProjects(data);
                            break;
                        case 16:
                            arrlistView[s].vdCheck = enableFrameBreaking(data).ToString();
                            //arr[s] = enableFrameBreaking(data);
                            break;
                        case 17:
                            arrlistView[s].vdCheck = xFrameOptions(data).ToString();
                            //   arr[s]=  xFrameOptions(data);
                            break;
                        case 18:
                            arrlistView[s].vdCheck = loginFirst(data).ToString();
                            break;
                        case 19:
                            arrlistView[s].vdCheck = serverSorting(data).ToString();
                            break;
                        case 20:
                            arrlistView[s].vdCheck = projectSorting(data).ToString();
                            break;

                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                labeldone.Text = e.Message.ToString();
            }
            Console.WriteLine("before color");

            string fileConfig = Path.GetFileName(filepath);
            string fileProp = Path.GetFileName(propFile);


            int lengthColor = 0;
            if (radioButton1.Checked == true)
            {
                lengthColor = 10;
            }
            else
            {
                lengthColor = arr.Length;
            }


            for (int i = 0; i <  lengthColor; i++)
            {
                if(arrlistView[i].vdCheck == "1")
                {
                    listView1.Items[i].SubItems[2].Text = "Yes";
                }
                else
                {
                    listView1.Items[i].SubItems[2].Text = "No";
                }

                if(arrlistView[i].inetPub == "1")
                {
                    listView1.Items[i].SubItems[3].Text = "Yes";
                }
                else
                {
                    listView1.Items[i].SubItems[3].Text = "No";
                }

                if(arrlistView[i].vdCheck == "1" || arrlistView[i].inetPub == "1")
                {
                    listView1.Items[i].SubItems[0].ForeColor = Color.Green;
                }else
                {
                    listView1.Items[i].SubItems[0].ForeColor = Color.Red;
                }
            }


            Console.WriteLine("TestSKS");
            }


        private void Form1_Load(object sender, EventArgs e)
        {
          

        }

        private void settingsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }



        public int requestFilteringOption(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("requestFiltering");


            for (int i = 0; i < elemList.Count; i++)
            {
                Console.WriteLine("xxxx");
                Console.WriteLine(elemList[i].InnerXml);

                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("no.Name");
                    Console.WriteLine(no.Name);

                    if (elemList[i].HasChildNodes == true)
                    {
                        foreach (XmlNode n in no)
                        {
                            Console.WriteLine(n.Name);


                            if (n.Attributes["verb"]?.InnerText == "OPTIONS")
                            {
                                return 1;
                                listView1.Items[2].SubItems[0].Text = "Yes";
                                listView1.Items[2].SubItems[2].Text = filepath;
                                listView1.Items[2].SubItems[0].ForeColor = Color.Green;

                                // n.Attributes["allowed"].Value = "true";
                                //   settingsListBox.SetSelected(2, true);
                                //  settingsListBox.SetItemChecked(2, true);
                                break;
                            }

                        }
                    }

                }
            }


            return 0;
//            doc.Save(filepath);

        }

        public int customError(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("system.web");

            /*
            listView1.Items[8].SubItems[0].Text = "No";
            listView1.Items[8].SubItems[2].Text = filepath;
            listView1.Items[8].SubItems[0].ForeColor = Color.Red;
            */

            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("no.Name");
                    Console.WriteLine(no.Name);
                    Label label1 = new Label();
                    if (no.Name.ToString() == "customErrors")
                    {
                        Console.WriteLine("inside custom error ");

                        Console.WriteLine(no.Attributes["defaultRedirect"]?.InnerText);
                        Console.WriteLine(no.Attributes["mode"]?.InnerText);
                        if(no.Attributes["defaultRedirect"]?.InnerText !="" && no.Attributes["mode"]?.InnerText == "RemoteOnly")
                        {
                            return 1;
                            listView1.Items[8].SubItems[0].Text = "Yes";
                            listView1.Items[8].SubItems[2].Text = filepath;
                            listView1.Items[8].SubItems[0].ForeColor = Color.Green;
                            //   settingsListBox.SetSelected(8, true);
                            //  settingsListBox.SetItemChecked(8, true);
                        }
                        // doc.Save(filepath);
                        break;
                    }
                }
            }

            return 0;
        }

        public int requestFilteringRules(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("requestFiltering");
            /*
              listView1.Items[1].SubItems[0].Text = "No";
              listView1.Items[1].SubItems[2].Text = filepath;
              listView1.Items[1].SubItems[0].ForeColor = Color.Red;
              */
            for (int i = 0; i < elemList.Count; i++)
            {             

                Console.WriteLine(elemList[i].InnerXml);

                //var x = elemList[i].InnerText;
                //Console.WriteLine(x);

                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("no.Name");
                    Console.WriteLine(no.Name);

                    if (no.Name.ToString() == "verbs")
                    {
                        Console.WriteLine("verbs ");

                        Console.WriteLine(no.Attributes["allowUnlisted"]?.InnerText);

                        if (no.Attributes["allowUnlisted"]?.InnerText != "")
                        {
                            return 1;
                            listView1.Items[1].SubItems[0].Text = "Yes";
                            listView1.Items[1].SubItems[2].Text = filepath;
                            listView1.Items[1].SubItems[0].ForeColor = Color.Green;
                            //  settingsListBox.SetSelected(1, true);
                            //   settingsListBox.SetItemChecked(1, true);
                            break;
                        }
                        else
                            {
                        
                        }
                    }

                }
            }
            //doc.Save(filepath);
            return 0;
        }
        public int hstsheader1(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
            /*
            listView1.Items[9].SubItems[0].Text = "No";
            listView1.Items[9].SubItems[2].Text = filepath;
            listView1.Items[9].SubItems[0].ForeColor = Color.Red;
            */
            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("no.Name");
                    Console.WriteLine(no.Name);

                    if (no.Name.ToString() == "rewrite")
                    {
                        Console.WriteLine("inside rewrite");

                        foreach (XmlNode n in no)
                        {

                            if (n.Name.ToString() == "outboundRules")
                            {
                                foreach (XmlNode m in n)
                                {
                                    if (m.Name.ToString() == "rule")
                                    {
                                        Console.WriteLine(m.Name);
                                        if (m.Attributes["name"]?.InnerText == "Add Strict-Transport-Security when HTTPS")
                                        {
                                                
                                            foreach (XmlNode o in m)
                                            {
                                                if (o.Name.ToString() == "match")
                                                {
                                                    //Console.WriteLine("inside match111");
                                                    ;
                                                    if (o.Attributes["serverVariable"]?.InnerText == "RESPONSE_Strict_Transport_Security" )
                                                    {
                                                        Console.WriteLine("serverVariable" + m.Attributes["serverVariable"]?.InnerText);
                                                        return 1;
                                                    }
                                                }

                                            }

                                        }

                                    }
                                }
                            }
                        }

                    }

                }
            }
            return 0;
        }

        public int hstsheader(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
            /*
            Console.WriteLine("hstsheader");
            listView1.Items[6].SubItems[0].Text = "No";
            listView1.Items[6].SubItems[2].Text = filepath;
            listView1.Items[6].SubItems[0].ForeColor = Color.Red;
            */

            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("hstsheadertags" +no.Name);
                    if (no.Name.ToString() == "httpProtocol")
                    {
                        foreach (XmlNode n in no)
                        {
                            Console.WriteLine(n.Name);
                            if (n.Name == "customHeaders")
                            {
                                Console.WriteLine("try1"+n.Name);
                               foreach(XmlNode m in n )
                                {
                                    Console.WriteLine("try2" + m.Name);
                                    if (m.Name == "add")
                                    {

                                        if (m.Attributes["name"]?.InnerText == "Add Strict-Transport-Security when HTTPS" || m.Attributes["name"]?.InnerText == "Strict-Transport-Security")//&& m.Attributes["value"]?.InnerText == "max-age=31536000")
                                        {
                                            return 1;
                                            listView1.Items[6].SubItems[0].Text = "Yes";
                                            listView1.Items[6].SubItems[2].Text = filepath;
                                            listView1.Items[6].SubItems[0].ForeColor = Color.Green;

                                            //     settingsListBox.SetSelected(6, true);
                                            //    settingsListBox.SetItemChecked(6, true);

                                        }
                                     

                                    }

                                }

                            }
                                }
                            }
                        }


                    }
                
            
            return 0;
        }


        public int timeOut(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("system.web");
            /*
            listView1.Items[7].SubItems[0].Text = "No";
            listView1.Items[7].SubItems[2].Text = filepath;
            listView1.Items[7].SubItems[0].ForeColor = Color.Red;
            */

            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("no.Name");
                    Console.WriteLine(no.Name);

                    if (no.Name.ToString() == "sessionState")
                    {
                        return 1;
                        listView1.Items[7].SubItems[0].Text = "Yes";
                        listView1.Items[7].SubItems[2].Text = filepath;
                        listView1.Items[7].SubItems[0].ForeColor = Color.Green;
                        //    settingsListBox.SetSelected(7, true);
                        //  settingsListBox.SetItemChecked(7, true);
                        Console.WriteLine("inside sessionState");

                        Console.WriteLine(no.Attributes["timeout"]?.InnerText);

                        // doc.Save(filepath);
                        break;
                    }

                }
            }
            return 0;
        }

        public int httpCookie(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
      

            Console.WriteLine("inside httpCookie");
            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {                  
                    Console.WriteLine(no.Name);

                    if (no.Name.ToString() == "rewrite")
                    {
                        foreach (XmlNode n in no)
                        {
                            Console.WriteLine(n.Name);
                            if (n.Name == "outboundRules")
                            {
                                Console.WriteLine("inside outboundRules");
                                foreach (XmlNode m in n)
                                {
                                    Console.WriteLine(m.Name);
                                    if (m.Name == "preConditions")
                                    {
                                        Console.WriteLine("inside preConditons");
                                        foreach(XmlNode o in m)
                                        {
                                            if(o.Name == "preCondition")
                                            {
                                                Console.WriteLine("inside precondition");
                                               
                                                    Console.WriteLine(o.Name);
                                                    if (o.Attributes["name"]?.InnerText == "No HttpOnly")//&& o.Attributes["input"]?.InnerText == "{RESPONSE_Set_Cookie}")
                                                    {
                                                        Console.WriteLine("httpcookie done");
                                                        return 1;
                                                        listView1.Items[3].SubItems[0].Text = "Yes";
                                                        listView1.Items[3].SubItems[2].Text = filepath;
                                                        listView1.Items[3].SubItems[0].ForeColor = Color.Green;
                                                        //      settingsListBox.SetSelected(3, true);
                                                        //        settingsListBox.SetItemChecked(3, true);
                                                    }
                                                
                                            }
                                         
                                            
                                          
                                        }
                                        break;
                                    }

                                }
                            }
                            
                         
                        }
                  }

                }
            }
            return 0;
        }

        public int enableVersionHeader(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("system.web");

          /*  listView1.Items[4].SubItems[0].Text = "No";
            listView1.Items[4].SubItems[2].Text = filepath;
            listView1.Items[4].SubItems[0].ForeColor = Color.Red;
            */

            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("no.Name");
                    Console.WriteLine(no.Name);

                    if (no.Name.ToString() == "httpRuntime")
                    {
                        Console.WriteLine("inside httpRuntime");

                        Console.WriteLine(no.Attributes["enableVersionHeader"]?.InnerText);
                        if (no.Attributes["enableVersionHeader"]?.InnerText== "false") // ideal case
                        {
                            return 1;
                            listView1.Items[4].SubItems[0].Text = "Yes";
                            listView1.Items[4].SubItems[2].Text = filepath;
                            listView1.Items[4].SubItems[0].ForeColor = Color.Green;
                            //    settingsListBox.SetSelected(4, true);
                            //   settingsListBox.SetItemChecked(4, true);
                        }

                        // doc.Save(filepath);
                        break;
                    }

                }
            }
            return 0;
        }

        public int hidingServer(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
            /*
            listView1.Items[9].SubItems[0].Text = "No";
            listView1.Items[9].SubItems[2].Text = filepath;
            listView1.Items[9].SubItems[0].ForeColor = Color.Red;
            */
            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("no.Name");
                    Console.WriteLine(no.Name);

                    if (no.Name.ToString() == "rewrite")
                    {
                        Console.WriteLine("inside rewrite");

                        foreach (XmlNode n in no)
                        {

                            if (n.Name.ToString() == "outboundRules")
                            {
                                foreach (XmlNode m in n)
                                {
                                    if (m.Name.ToString() == "rule")
                                    {
                                        Console.WriteLine(m.Name);
                                        if (m.Attributes["name"]?.InnerText == "RESPONSE_SERVER")
                                        {
                                             return 1;
                                        }

                                    }
                                }
                            }
                        }
                        // doc.Save(filepath);

                        break;
                    }

                }
            }
            return 0;
        }


        public int urlScan(XmlDataDocument doc)
        {
            //not to use this
            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
/*
            listView1.Items[0].SubItems[0].Text = "No";
            listView1.Items[0].SubItems[2].Text = filepath;
            listView1.Items[0].SubItems[0].ForeColor = Color.Red;

            */
            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("no.Name");
                    Console.WriteLine(no.Name);

                    if (no.Name.ToString() == "security")
                    {
                        Console.WriteLine("inside security");

                        foreach (XmlNode n in no)
                        {

                            if (n.Name.ToString() == "requestFiltering")
                            {
                                foreach (XmlNode m in n)
                                {
                                    if (m.Name.ToString() == "filteringRules")
                                    {
                                        foreach (XmlNode o in m )
                                        {
                                            if (o.Name.ToString() == "filteringRule")
                                            {
                                                if (o.Attributes["scanUrl"]?.InnerText=="true")
                                                {
                                                    return 1;
                                                    listView1.Items[0].SubItems[0].Text = "Yes";
                                                    listView1.Items[0].SubItems[2].Text = filepath;
                                                    listView1.Items[0].SubItems[0].ForeColor = Color.Green;
                                                    //  settingsListBox.SetSelected(0, true);
                                                    // settingsListBox.SetItemChecked(0, true);
                                                }
                                            }
                                            
                                        }
                                    }
                                }
                            }
                        }
                        // doc.Save(filepath);
                        break;
                    }

                }
            }
            return 0;
        }


        public int hidingAspVersion(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
            /*
            listView1.Items[9].SubItems[0].Text = "No";
            listView1.Items[9].SubItems[2].Text = filepath;
            listView1.Items[9].SubItems[0].ForeColor = Color.Red;
            */
            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("no.Name");
                    Console.WriteLine(no.Name);

                    if (no.Name.ToString() == "rewrite")
                    {
                        Console.WriteLine("inside rewrite");

                        foreach (XmlNode n in no)
                        {

                            if (n.Name.ToString() == "outboundRules")
                            {
                                foreach (XmlNode m in n)
                                {
                                    if (m.Name.ToString() == "rule")
                                    {
                                        Console.WriteLine(m.Name);
                                        if (m.Attributes["name"]?.InnerText == "RESPONSE_X-ASPNET-VERSION")
                                        {
                                            
                                            foreach (XmlNode o in m)
                                            {
                                                if(o.Name.ToString() == "match")
                                                {
                                                    Console.WriteLine("inside match111");
                                                    Console.WriteLine("serverVariable" + o.Attributes["serverVariable"]?.InnerText);
                                                    if (o.Attributes["serverVariable"]?.InnerText == "RESPONSE_X-ASPNET-VERSION" && o.Attributes["pattern"]?.InnerText == ".*")
                                                    {
                                                        Console.WriteLine("serverVariable" + m.Attributes["serverVariable"]?.InnerText);
                                                        return 1;
                                                    }
                                                }
                                               
                                            }
                                                
                                        }

                                    }
                                }
                            }
                        }
                        // doc.Save(filepath);

                        break;
                    }

                }
            }
            return 0;
        }

        public int hidingServerResponse(XmlDataDocument doc)
        {

            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
            /*
            listView1.Items[9].SubItems[0].Text = "No";
            listView1.Items[9].SubItems[2].Text = filepath;
            listView1.Items[9].SubItems[0].ForeColor = Color.Red;
            */
            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("no.Name");
                    Console.WriteLine(no.Name);

                    if (no.Name.ToString() == "rewrite")
                    {
                        Console.WriteLine("inside rewrite");

                        foreach (XmlNode n in no)
                        {

                            if (n.Name.ToString() == "outboundRules")
                            {
                                foreach (XmlNode m in n)
                                {
                                    if (m.Name.ToString() == "rule")
                                    {
                                        Console.WriteLine(m.Name);
                                        if (m.Attributes["name"]?.InnerText == "RESPONSE_X-POWERED-BY")
                                        {

                                            foreach (XmlNode o in m)
                                            {
                                                if (o.Name.ToString() == "match")
                                                {
                                                    //Console.WriteLine("inside match111");
                                                  ;
                                                    if (o.Attributes["serverVariable"]?.InnerText == "RESPONSE_X-POWERED-BY" && o.Attributes["pattern"]?.InnerText == ".*")
                                                    {
                                                        Console.WriteLine("serverVariable" + m.Attributes["serverVariable"]?.InnerText);
                                                        return 1;
                                                    }
                                                }

                                            }

                                        }

                                    }
                                }
                            }
                        }

                    }

                }
            }
            return 0;

        }


        private void label1_Click(object sender, EventArgs e)
        {
                
        }


        private void properties1()
        {
            var data = new Dictionary<string, string>();
            foreach (var row in File.ReadAllLines(propFile))
                data.Add(row.Split('=')[0], string.Join("=", row.Split('=').Skip(1).ToArray()));

            try
            {
                foreach (KeyValuePair<string, string> kvp in data)
                {
                    //textBox3.Text += ("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
                    Console.WriteLine("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
                }
            }
            catch (Exception e)
            {
                labeldone.Text = e.Message.ToString();
            }
        }

    
        private int includeTimeStamp(Dictionary<string,string> data)
        {
            Console.WriteLine("inside includeTimeStamp");
            String value;
            if (data.ContainsKey("includeTimeStamp"))
            {
                return 1;
                value = data["includeTimeStamp"];
                Console.WriteLine(value);
                listView1.Items[11].SubItems[0].Text = "Yes";
                listView1.Items[11].SubItems[2].Text = propFile;
                listView1.Items[11].SubItems[0].ForeColor = Color.Green;
                //   settingsListBox.SetSelected(9, true);
                //   settingsListBox.SetItemChecked(9, true);
            }
            else
            {
                return 0;

                listView1.Items[11].SubItems[0].Text = "No";
                listView1.Items[11].SubItems[2].Text = propFile;
                listView1.Items[11].SubItems[0].ForeColor = Color.Red;
                Console.WriteLine("Key Not Present");
               // return;
            }
        }

        private int includeSessionUrl(Dictionary<string, string> data)
        {
            Console.WriteLine("inside includeSessionUrl");
            String value;
            if (data.ContainsKey("includeSessionUrl"))
            {
                return 1;
                value = data["includeSessionUrl"];
                Console.WriteLine(value);
                listView1.Items[12].SubItems[0].Text = "Yes";
                listView1.Items[12].SubItems[2].Text = propFile;
                listView1.Items[12].SubItems[0].ForeColor = Color.Green;
                //   settingsListBox.SetSelected(10, true);
                //  settingsListBox.SetItemChecked(10, true);
            }
            else
            {
                return 0;
                listView1.Items[12].SubItems[0].Text = "No";
                listView1.Items[12].SubItems[2].Text = propFile;
                listView1.Items[12].SubItems[0].ForeColor = Color.Red;
                Console.WriteLine("Key Not Present");
              //  return;
            }
        }

        private int changePwd(Dictionary<string, string> data)
        {
            Console.WriteLine("inside changePwd");
            String value;
            if (data.ContainsKey("changePwd"))
            {
                return 1;
                value = data["changePwd"];
                Console.WriteLine(value);
                listView1.Items[13].SubItems[0].Text = "Yes";
                listView1.Items[13].SubItems[2].Text = propFile;
                listView1.Items[13].SubItems[0].ForeColor = Color.Green;
                // settingsListBox.SetSelected(11, true);
                //settingsListBox.SetItemChecked(11, true);
            }
            else
            {
                return 0;
                listView1.Items[13].SubItems[0].Text = "No";
                listView1.Items[13].SubItems[2].Text = propFile;
                listView1.Items[13].SubItems[0].ForeColor = Color.Red;
                Console.WriteLine("Key Not Present");
              //  return;
            }
        }

        private int allowSeamlessLogin(Dictionary<string, string> data)
        {
            Console.WriteLine("inside allowSeamlessLogin");
            String value;
            if (data.ContainsKey("allowSeamlessLogin"))
            {
                return 1;

                value = data["allowSeamlessLogin"];
                Console.WriteLine(value);
                // settingsListBox.SetSelected(12, true);
                //settingsListBox.SetItemChecked(12, true);
                listView1.Items[14].SubItems[0].Text = "Yes";
                listView1.Items[14].SubItems[2].Text = propFile;
                listView1.Items[14].SubItems[0].ForeColor = Color.Green;
            }
            else
            {
                return 0;
                listView1.Items[14].SubItems[0].Text = "No";
                listView1.Items[14].SubItems[2].Text = propFile;
                listView1.Items[14].SubItems[0].ForeColor = Color.Red;
                Console.WriteLine("Key Not Present");
              //  return;
            }
        }

        private int allowLoginByUrl(Dictionary<string, string> data)
        {
            Console.WriteLine("inside allowLoginByUrl");
            String value;
            if (data.ContainsKey("allowLoginByUrl"))
            {
                value = data["allowLoginByUrl"];
                return 1;
                /*
                Console.WriteLine(value);
                listView1.Items[15].SubItems[0].Text = "Yes";
                listView1.Items[15].SubItems[2].Text = propFile;
                listView1.Items[15].SubItems[0].ForeColor = Color.Green;
                */
                //settingsListBox.SetSelected(13, true);
                //settingsListBox.SetItemChecked(13, true);
            }
            else
            {
                return 0;
                /*
                listView1.Items[15].SubItems[0].Text = "No";
                listView1.Items[15].SubItems[2].Text = propFile;
                listView1.Items[15].SubItems[0].ForeColor = Color.Red;
                Console.WriteLine("Key Not Present");
                return;
                */
            }
        }

        private int allowAdminApplyAllProjects(Dictionary<string, string> data)
        {
            Console.WriteLine("inside allowAdminApplyAllProjects");
            String value;
            if (data.ContainsKey("allowAdminApplyAllProjects"))
            {

                return 1;
                /*
                value = data["allowAdminApplyAllProjects"];
                Console.WriteLine(value);
                listView1.Items[16].SubItems[0].Text = "Yes";
                listView1.Items[16].SubItems[2].Text = propFile;
                listView1.Items[16].SubItems[0].ForeColor = Color.Green;
                */
                // settingsListBox.SetSelected(14, true);
                //   settingsListBox.SetItemChecked(14, true);
            }
            else
            {
                return 0;
                /*
                Console.WriteLine("Key Not Present");
                listView1.Items[16].SubItems[0].Text = "No";
                listView1.Items[16].SubItems[2].Text = propFile;
                listView1.Items[16].SubItems[0].ForeColor = Color.Red;
                return;
                */
            }
        }

        private int enableFrameBreaking(Dictionary<string, string> data)
        {
            Console.WriteLine("inside enableFrameBreaking");
            String value;
            if (data.ContainsKey("enableFrameBreaking"))
            {
                return 1;
                /*
                value = data["enableFrameBreaking"];
                Console.WriteLine(value);
                listView1.Items[17].SubItems[0].Text = "Yes";
                listView1.Items[17].SubItems[2].Text = propFile;
                listView1.Items[17].SubItems[0].ForeColor = Color.Green;

            */
                //  settingsListBox.SetSelected(15, true);
                // settingsListBox.SetItemChecked(15, true);
            }
            else
            {
                return 0;
                /*
                listView1.Items[17].SubItems[0].Text = "No";
                listView1.Items[17].SubItems[2].Text = propFile;
                listView1.Items[17].SubItems[0].ForeColor = Color.Red;
                Console.WriteLine("Key Not Present");
                return;
                */
            }
        }

        private int xFrameOptions(Dictionary<string, string> data)
        {
            Console.WriteLine("inside xFrameOptions");
            String value;
            if (data.ContainsKey("xFrameOptions"))
            {
                value = data["xFrameOptions"];
                Console.WriteLine(value);
                //   settingsListBox.SetSelected(16, true);
                //  settingsListBox.SetItemChecked(16, true);

                return 1;
              //  listView1.Items[18].SubItems[0].Text = "Yes";
               // listView1.Items[18].SubItems[0].ForeColor = Color.Green;
               // listView1.Items[18].SubItems[2].Text = propFile;

                 
            //    ImageList imageListSmall = new ImageList();
             //   imageListSmall.Images.Add(Bitmap.FromFile("C:\\Users\\ss19405\\Documents\\CodeBase\\Test1\\Test1\\Properties\\check.bmp"));
             //   listView1.SmallImageList = imageListSmall;
             //   this.Controls.Add(listView1);


            }
            else
            {
                return 0;
              //  listView1.Items[18].SubItems[0].Text = "No";
               // listView1.Items[18].SubItems[0].ForeColor = Color.Red;
              //  listView1.Items[18].SubItems[2].Text = propFile;

              //  Console.WriteLine("Key Not Present");
              //  return;
            }
        }

        private int loginFirst(Dictionary<string, string> data)
        {
            Console.WriteLine("inside loginFirst");
            String value;
            if (data.ContainsKey("loginFirst"))
            {
                return 1;    
            }
            else
            {
                return 0;

            }
        }

        private int serverSorting(Dictionary<string, string> data)
        {
            Console.WriteLine("inside serverSorting");
            String value;
            if (data.ContainsKey("serverSorting"))
            {
                return 1;

            }
            else
            {
                return 0;
            
            }
        }

        private int projectSorting(Dictionary<string, string> data)
        {
            Console.WriteLine("inside projectSorting");
            String value;
            if (data.ContainsKey("projectSorting"))
            {
                return 1;

            }
            else
            {
                return 0;

            }
        }

        private void CreateMyListView(ListView listView1)
        {
            // Create a new ListView control.
           
            listView1.Bounds = new Rectangle(new Point(21, 90), new Size(900, 410));

            // Set the view to show details.
            listView1.View = System.Windows.Forms.View.Details;
            // Allow the user to edit item text.
            //listView1.LabelEdit = true;
            // Allow the user to rearrange columns.
            listView1.AllowColumnReorder = true;
            // Display check boxes.
           // listView1.CheckBoxes = true;
            // Select the item and subitems when selection is made.
            listView1.FullRowSelect = true;
            // Display grid lines.
            listView1.GridLines = true;
            // Sort the items in the list in ascending order.
            //listView1.Sorting = SortOrder.Ascending;


            // Create three items and three sets of subitems for each item.


            // Place a check mark next to the item.
            //  item1.SubItems.Add("1"); //tag wala
            // item1.SubItems.Add("and"); //filepath

            //ListViewItem item2 = new ListViewItem("", 1);
            // item2.SubItems.Add("2");

            //  ListViewItem item3 = new ListViewItem("", 2);
            // item3.SubItems.Add("3");


            // Create columns for the items and subitems.
            // Width of -2 indicates auto-size.
            //  string status = "status";
            //   status.fon
            //  status = new Font(defaultFont.FontFamily, defaultFont.Size, FontStyle.Bold);

            Font defaultFont = SystemFonts.DefaultFont;
            //        status = new Font(defaultFont.FontFamily, defaultFont.Size, FontStyle.Bold);
            listView1.Columns.Add("S.No.", -2, HorizontalAlignment.Left ); //tick cross 
            listView1.Columns.Add("Setting", -2, HorizontalAlignment.Left); //tag name
            listView1.Columns.Add("VD   ", -2, HorizontalAlignment.Left); //FileName
            listView1.Columns.Add("InetPub ", -2, HorizontalAlignment.Left);
            
            //listView1.Items[0].SubItems[0].Font = new Font(defaultFont.FontFamily, defaultFont.Size, FontStyle.Bold);



            //Add the items to the ListView.
            //   listView1.Items.AddRange(new ListViewItem[] { item1, item2, item3 }); //list
            // listView1.Items.Add(new ListViewItem(new [] { tagName}) );


            //   listView1.Items.Add(item);

            // Create two ImageList objects.

            // Add the ListView to the control collection.
            this.Controls.Add(listView1);
        }

       
        /*

        private void Form1_FormClosing(object sender, FormClosedEventArgs e)
        {
            // Determine if text has changed in the textbox by comparing to original text.

            if (e.CloseReason == CloseReason.UserClosing)
            {   

                Main main1 = new Main();

                this.Hide();

                main1.Closed += (s, args) => this.Close();
                //  form2.Show();
                // Show the settings form
                main1.Show();
            }

        }
        */

        private void cleanPath(string path)
        {
            File.WriteAllLines(path, File.ReadAllLines(path).Where(l => !string.IsNullOrWhiteSpace(l)));
            //string.Join(" ", File.ReadAllText(path, Encoding.Default).Split(new string[] { " ", "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries))
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
          
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
