﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Web;
using System.Xml.Linq;

namespace Test1
{
    public partial class ConfigurationForm : Form
    {
        public String filepath;
        public string propBackupPath;
        public string xmlBackupPath;
        ListView listView1 = new ListView();
        ListViewRow[] arrlistView;
        public String propFile;      
        TextBox txtBox1 = new TextBox();
        int[] checkListArr = new int[21];
        private  Dictionary<string,string> data = new Dictionary<string, string>();
        private ListViewItem viewItem;
        private int subItemIndex = 0;
        XmlDataDocument doc1 = new XmlDataDocument();
        public String inetPath;
        int flag = 0;

        Form1 form1 = new Form1();
        

        public ConfigurationForm()
        {
            InitializeComponent();

            CreateMyListView();

            this.AutoSize = true;
            //   listView1.KeyDown += listView_KeyDown;

            textBox1.GotFocus +=   RemoveText;
            textBox1.LostFocus += AddText;
            textBox1.Text = "Enter TimeOut";
            textBox1.TextAlign = HorizontalAlignment.Center;

            textBox2.GotFocus += RemoveText1;
            textBox2.LostFocus += AddText1;
            textBox2.Text = "Enter link for custom error webpage";
            textBox2.TextAlign = HorizontalAlignment.Center;


            string[] serianlNo = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18","19", "20", "21" };
            //string[] tagName = { "Sample 1", "Sample 2", "Sample 3" };

            String[] settingName =
            {   //urlscan
                "Request Filtering Rule",
                "Disable OPTION Method",
                "Enabling Secure Flag/HTTPOnly cookie",
                "Hide Programming Language/Version",
                "Hiding Server/Powered By",
                "Adding HSTS Header",
                "Timeout setting",
                "Custom Error Messages",
                "hidingAspVersion",
                "hidingServerresponse",
                "Include time stamp in error message",
                "Session information is included on the URL",
                "Allow users to change expired password",
                "Allow automatic login if session is lost",
                "Users to log including their UserID and Pass in URL",
                "Users the ‘Web administrator’ privilege",
                "Prevent clickjacking by adding a frame - breaking script to pages",
                "Prevent clickjacking by adding an X - Frame - Options: SAMEORIGIN header to page response",
                "loginFirst",
                "serverSorting",
                "projectSorting"
            };
            
            for (int i = 0; i < settingName.Length; i++)
            {
                listView1.Items.Add(new ListViewItem(new[] {"", serianlNo[i], settingName[i]}));

            }


          
            ListViewRow objListView = new ListViewRow();
            arrlistView = new ListViewRow[settingName.Length];

            for (int i = 0; i < settingName.Length; i++)
            {
                ListViewRow obj = new ListViewRow();
                obj.settingName = settingName[i];
                obj.vdCheck = "NA";
                obj.inetPub = "NA";
                arrlistView[i] = obj;

            }

            listView1.Controls.Add(txtBox1);
            txtBox1.Visible = false;
            txtBox1.KeyPress += (sender, args) =>
            {
                TextBox textBox = sender as TextBox;

                if ((int)args.KeyChar == 13)
                {
                    if (viewItem != null)
                    {
                        viewItem.SubItems[subItemIndex].Text = textBox.Text;
                        Console.WriteLine("textBox" + textBox.Text);
                    }
                    textBox.Visible = false;
                }
            };

            /*
            listView1.Controls.Add(txtBox1);
            txtBox1.Visible = false;
            txtBox1.KeyPress += (sender, args) =>
            {
                TextBox textBox = sender as TextBox;

                if ((int)args.KeyChar == 13)
                {
                    if (viewItem != null)
                    {
                        viewItem.SubItems[subItemIndex].Text = textBox.Text;
                    }
                    textBox.Visible = false;
                }
            };
            */
           
        }




        public void RemoveText(object sender, EventArgs e)
        {
            if (textBox1.Text == "Enter TimeOut")
            {
                textBox1.Text = "";
            }
        }

        public void AddText(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBox1.Text))
                textBox1.Text = "Enter TimeOut";
        }


        public void RemoveText1(object sender, EventArgs e)
        {
            if (textBox2.Text == "Enter link for custom error webpage")
            {
                textBox2.Text = "";
            }   
        }

        public void AddText1(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBox2.Text))
                textBox2.Text = "Enter link for custom error webpage";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //   Response.redirect("");
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                filepath = folderBrowserDialog1.SelectedPath;

            }
            propBackupPath = filepath;
            xmlBackupPath = filepath;


            if (filepath != null )
            {
                filepath = Path.Combine(filepath, "web.config");
                vdLabel.Text = filepath;
            }
            else
            {
                MessageBox.Show("Please select filepath", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }        
        }

        private void CreateMyListView()
        {
            // Create a new ListView control.
            ListView checkList = new ListView();
            checkList.CheckBoxes = true;

            listView1.Bounds = new Rectangle(new Point(21, 90), new Size(900, 450));
            // Set the view to show details.
            listView1.View = System.Windows.Forms.View.Details;
            // Allow the user to edit item text.
            listView1.LabelEdit = true;
            // Allow the user to rearrange columns.
            listView1.AllowColumnReorder = true;
            // Display check boxes.
            listView1.CheckBoxes = true;
            // Select the item and subitems when selection is made.
            listView1.FullRowSelect = true;
            // Display grid lines. 
            listView1.GridLines = true;

            listView1.HideSelection = false;
            listView1.Focus();
            
            

            // Sort the items in the list in ascending order.
            //listView1.Sorting = SortOrder.Ascending;
            // Create three items and three sets of subitems for each item.
            listView1.Scrollable = false;
            listView1.Columns.Add("CheckList", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("S.No", -2, HorizontalAlignment.Left);//tick cross 
            listView1.Columns.Add("Setting", -2, HorizontalAlignment.Left);
            //tag name
            //    listView1.Columns.Add("VD  ", -2, HorizontalAlignment.Left); //FileName
            //      listView1.Columns.Add("Value ", -2, HorizontalAlignment.Left);
            // this.listView1.SelectedItems[0].BeginEdit();
            listView1.Columns[0].DisplayIndex = listView1.Columns.Count - 1; //for keeping 
            listView1.Invalidate();
            this.Controls.Add(listView1);



        }



        private void ConfigurationForm_Load(object sender, EventArgs e)
        {

        }

        /*
        private void listView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.F2 && listView1.SelectedItems.Count > 0)
            {
                viewItem = listView1.SelectedItems[0];
                var bounds = viewItem.Bounds;
                var col2_bounds = viewItem.SubItems[3].Bounds;
                var col1_bounds = viewItem.SubItems[2].Bounds;
                col1_bounds.Width -= col2_bounds.Width;

                subItemIndex = 3;
                txtBox1.SetBounds(col2_bounds.X, bounds.Y, col2_bounds.Width, bounds.Height);
                /*
                if (xpos > col2_bounds.X)
                {
                    subItemIndex = 2;
                    txtBox1.SetBounds(col2_bounds.X, bounds.Y, col2_bounds.Width, bounds.Height);
                }
                else
                {
                    subItemIndex = 0;
                    txtBox1.SetBounds(col1_bounds.X, bounds.Y, col1_bounds.Width, bounds.Height);
                }
                
                txtBox1.Text = string.Empty;
                txtBox1.Visible = true;
            }
        }
        private void listView_Click(object sender, EventArgs e)
        {
           // xpos = MousePosition.X - listView1.PointToScreen(Point.Empty).X;
            Console.WriteLine("xpos" + xpos);
        }
        */

        private void Set_Click(object sender, EventArgs e)
        {
            Console.WriteLine("filepath1"+filepath);
            try
            {
                cleanPath(filepath);
            }
            catch(Exception exp)
            {
                Console.WriteLine("cleanpath" + exp.ToString());
            }
            
            ListView.CheckedListViewItemCollection checkedItems = listView1.CheckedItems;


            Console.WriteLine("checkedList items");
            foreach (ListViewItem item in checkedItems)
            {
                checkListArr[item.Index] = 1;
                Console.WriteLine(item.Index);
                //Console.WriteLine(item.Index);
            }

            Console.WriteLine("radioButton1"+radioButton1.Checked.ToString());



            if ( (checkListArr[7] == 1 && textBox2.Text == "Enter link for custom error webpage") || (checkListArr[6] == 1 && textBox2.Text == "Enter TimeOut"))
            {
                MessageBox.Show("Please Enter Value in TimeBox or Custom Error Box", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (radioButton1.Checked == false)
                {
                    readDic();
                    sysDefault();
                }

                webConfig();
            }

            //changes to be done here
          
        }


        private void webConfig()
        {

            XmlDataDocument doc = new XmlDataDocument();
            XmlDataDocument doc1 = new XmlDataDocument();


            if (filepath != String.Empty && inetPath != String.Empty)
            {
                try {
               
                    try
                    {
                        String xmlData = File.ReadAllText(filepath);
                        doc.LoadXml(xmlData);

                        //for inetPub xml
                        String xmlData1 = File.ReadAllText(inetPath);
                        doc1.LoadXml(xmlData1);

                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                } catch (Exception e) {
                    MessageBox.Show("Web.Config file seems corrupt.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }



            xmlBackupPath = Path.Combine(xmlBackupPath, "BackUp");
            xmlBackupPath = Path.Combine(xmlBackupPath, "Config");

            //checkxmlBackuppath and then call the createDirectory function

            Console.WriteLine("xmlbackupPath" +xmlBackupPath);
            createDirectory(xmlBackupPath);
            //copyinng backup file
            outBounds(doc);
            backUpFileXml(xmlBackupPath);
            //xmlBackupPath;


            //addASPVersionNodes(doc);
            //nodeHidingServerResponse(doc);

            /*
                             "<filteringRules>" +
                              +
                              "<filteringRule name = 'SQL_Injection_Mitigation' scanUrl ='true' scanQueryString = 'true'>" +
                                  "<scanHeaders>" +
                                    "<clear>" +
                                   "<scanHeaders/>" +
                                   "<appliesTo>" +
                                     "<clear/>" +
                                     "<add fileExtension = '.asp'/>" +
                                     "<add fileExtension = '.aspx/>" +
                                   "<appliesTo/>" +
                                   "<denyStrings>" +
                                    "<add string = 'Select'/>" +
                                    "<add string = 'Drop'/>" +
                                     "<add string = 'Update'/>" +
                                     "<add string = 'Convert'/>" +
                                     "<add string = 'Alter'/>" +
                                     "<add string = 'Insert'/>" +
                                     "<add string = 'Count'/>" +
                                     "<add string = 'Delete'/>" +
                                   "<denyStrings/>" +
                                 "<filteringRule/>" +
                               "<filteringRules/>";
                */


            //creating backup file

            //creating backup directory


            // backUpFileProperties(newDirPath);
            Console.WriteLine("checkList");

            for (int i = 0; i < checkListArr.Length; i++)
            {
                Console.WriteLine(checkListArr[i]);

            }

            for (int i = 0; i < 10; i++)
            {
                switch (i)
                {
                    case 0:
                        if (checkListArr[i] == 1){
                            
                            if (form1.requestFilteringRules(doc1) == 0)
                            {
                                Console.WriteLine("requestFilteringRules");
                                requestFilteringRules(doc);
                            }                                 
                          

                        }
                        break;
                    case 1:
                        if (checkListArr[i] == 1)
                        {
                            if (form1.requestFilteringOption(doc1) == 0)
                            {

                                requestFilteringOption(doc);
                            }
                        }
                        break;
                    case 2:
                        if (checkListArr[i] == 1)
                        {

                            //use form function
                            if (form1.httpCookie(doc1) == 0)
                            {
                                CheckhttpCookie(doc);
                                //HTTPCookie(doc);
                            }
                        }
                        break;
                    case 3:
                        if (checkListArr[i] == 1)
                        {
                            if (form1.enableVersionHeader (doc1) == 0)
                            {
                                enableVersionHeader(doc);
                            }
                        }
                        break;
                    case 4:
                        if (checkListArr[i] == 1)
                        {
                            //add form 
                            if (form1.hidingServer (doc1) == 0)
                            {
                                CheckhidingServer(doc);

                                //HidingServer(doc);
                            }
                        }
                        break;
                    case 5:
                        if (checkListArr[i] == 1)
                        {
                            if ((form1.hstsheader(doc1) | form1.hstsheader1(doc1)) == 0)
                            {

                                if ((Checkhstsheader(doc) | Checkhstsheader1(doc)) == 1)
                                {
                                    continue;
                                }
                                else
                                {
                                    writeHstsheader(doc);
                                }

                            }   
                        }
                        break;
                    case 6:
                        if (form1.timeOut(doc1) == 0)
                        {
                            if (checkListArr[i] == 1)
                            {
                                timeOut(doc);
                            }
                        }
                        break;
                    case 7:
                        if (form1.customError(doc1) == 0)
                        {
                            if (checkListArr[i] == 1)
                            {
                                customError(doc);
                            }
                        }
                        break;
                    case 8:
                        if (form1.hidingAspVersion(doc1) == 0)
                        {
                            if (checkListArr[i] == 1)
                            {
                                ///add form 
                                CheckhidingAspVersion(doc);
                                //ASPVersionNode(doc);                   
                            }
                        }
                        break;
                    case 9:
                        if (form1.hidingServerResponse(doc1) == 0)
                        {
                            if (checkListArr[i] == 1)
                            {
                                //add form
                                CheckinghidingServerResponse(doc); //done
                                                                   // HidingServerResponse(doc);

                            }
                        }
                            break;
                        
                }

            }
            doc.Save(filepath);
            done.Text = "Done";
        }

        private void sysDefault()
        {
            Console.WriteLine("keys" + data.Keys);

            /*
            List<string> keyList = new List<string>(this.data.Keys);
            foreach (string i in keyList)
            {
                Console.WriteLine(i);
            }
            */
            var arrayOfAllKeys = data.Keys.ToArray();

            Console.WriteLine("keys array");
            foreach (string i in arrayOfAllKeys)
            {
                Console.WriteLine(i);
            }


            for (int i = 10; i < checkListArr.Length; i++)
            {
                switch (i)
                {
                    case 10:
                        //includeTimeStamp     

                        Console.WriteLine(Array.IndexOf(arrayOfAllKeys, "includeTimeStamp"));
                        if (arrayOfAllKeys.Contains("includeTimeStamp")) // (data.ContainsKey("includeTimeStamp"))//(arrayOfAllKeys.ToList().Contains("includeTimeStamp"))//(arrayOfAllKeys.Any("includeTimeStamp".Contains))  (arrayOfAllKeys.Contains("includeTimeStamp")) // (data.ContainsKey("includeTimeStamp"))
                        {

                            if (checkListArr[i] == 1)
                            {
                                data["includeTimeStamp"] = "1";
                            }else
                            {
                               // data["includeTimeStamp"] = "0";
                            }
                        }else
                        {
                            if (checkListArr[i] == 1)
                            {
                                data.Add("includeTimeStamp", "1");                             
                            }
                            else
                            {
                                //data.Add("includeTimeStamp", "0");
                            }                       
                        }
                     
                        break;
                    case 11:
                        //includeSessionUrl
                        //it should be zero
                        if (arrayOfAllKeys.Contains("includeSessionUrl")) //(data.ContainsKey("includeSessionUrl"))
                        {
                            if (checkListArr[i] == 1)
                            {
                                data["includeSessionUrl"] = "0";
                            }
                            else
                            {
                             //   data["includeSessionUrl"] = "0";
                            }
                        }
                        else
                        {
                            if (checkListArr[i] == 1)
                            {
                                data.Add("includeSessionUrl", "0");
                            }
                            else
                            {
                             //   data.Add("includeSessionUrl", "0");
                            }
                        }

                        break;
                    case 12:
                        //changePwd
                        if (data.ContainsKey("changePwd"))
                        {
                            if (checkListArr[i] == 1)
                            {
                                data["changePwd"] = "1";
                            }
                            else
                            {
                             //   data["changePwd"] = "0";
                            }
                        }
                        else
                        {
                            if (checkListArr[i] == 1)
                            {
                                data.Add("changePwd", "1");
                            }
                            else
                            {
                            //    data.Add("changePwd", "0");
                            }
                        }
                        break;
                    case 13:
                        //allowSeamlessLogin
                        if (data.ContainsKey("allowSeamlessLogin"))
                        {
                            if (checkListArr[i] == 1)
                            {
                                data["allowSeamlessLogin"] = "1";
                            }
                            else
                            {
                            //    data["allowSeamlessLogin"] = "0";
                            }
                        }
                        else
                        {
                            if (checkListArr[i] == 1)
                            {
                                data.Add("allowSeamlessLogin", "1");
                            }
                            else
                            {
                            //    data.Add("allowSeamlessLogin", "0");
                            }
                        }
                        break;
                    case 14:
                        //allowLoginByUrl
                        if (data.ContainsKey("allowLoginByUrl"))
                        {
                            if (checkListArr[i] == 1)
                            {
                                data["allowLoginByUrl"] = "0";
                            }
                            else
                            {
                            //    data["allowLoginByUrl"] = "0";
                            }
                        }
                        else
                        {
                            if (checkListArr[i] == 1)
                            {
                                data.Add("allowLoginByUrl", "0");
                            }
                            else
                            {
                             //   data.Add("allowLoginByUrl", "0");
                            }
                        }
                        break;
                    case 15:
                        //allowAdminApplyAllProjects
                        if (data.ContainsKey("allowAdminApplyAllProjects"))
                        {
                            Console.WriteLine("exits");
                            if (checkListArr[i] == 1)
                            {
                                data["allowAdminApplyAllProjects"] = "0";
                            }
                            else
                            {
                            //    data["allowAdminApplyAllProjects"] = "0";
                            }
                        }
                        else
                        {
                             Console.WriteLine("don't exits");
                            if (checkListArr[i] == 1)
                            {
                                data.Add("allowAdminApplyAllProjects", "0");
                            }
                            else
                            {
                             //   data.Add("allowAdminApplyAllProjects", "0");
                            }
                        }
                        break;
                    case 16:
                        //enableFrameBreaking
                        if (data.ContainsKey("enableFrameBreaking"))
                        {
                            if (checkListArr[i] == 1)
                            {
                                data["enableFrameBreaking"] = "1";
                            }
                            else
                            {
                            //    data["enableFrameBreaking"] = "0";
                            }
                        }
                        else
                        {
                            if (checkListArr[i] == 1)
                            {
                                data.Add("enableFrameBreaking", "1");
                            }
                            else
                            {
                            //    data.Add("enableFrameBreaking", "0");
                            }
                        }
                        break;
                    case 17:
                        //xFrameOptions
                        if (data.ContainsKey("xFrameOptions"))
                        {
                            if (checkListArr[i] == 1)
                            {
                                data["xFrameOptions"] = "2";
                            }
                            else
                            {
                             //   data["xFrameOptions"] = "0";
                            }
                        }
                        else
                        {
                            if (checkListArr[i] == 1)
                            {
                                data.Add("xFrameOptions", "1");
                            }
                            else
                            {
                             //   data.Add("xFrameOptions", "0");
                            }
                        }
                        break;
                    case 18:
                        //xFrameOptions
                        if (data.ContainsKey("loginFirst"))
                        {
                            if (checkListArr[i] == 1)
                            {
                                data["loginFirst"] = "1";
                            }
                            else
                            {
                          //      data["loginFirst"] = "0";
                            }
                        }
                        else
                        {
                            if (checkListArr[i] == 1)
                            {
                                data.Add("loginFirst", "1");
                            }
                            else
                            {
                             //   data.Add("loginFirst", "0");
                            }
                        }
                        break;
                    case 19:
                        //xFrameOptions
                        if (data.ContainsKey("serverSorting"))
                        {
                            if (checkListArr[i] == 1)
                            {
                                data["serverSorting"] = "2";
                            }
                            else
                            {
                             //   data["serverSorting"] = "0";
                            }
                        }
                        else
                        {
                            if (checkListArr[i] == 1)
                            {
                                data.Add("serverSorting", "2");
                            }
                            else
                            {
                           //     data.Add("serverSorting", "0");
                            }
                        }
                        break;
                    case 20:
                        //xFrameOptions
                        if (data.ContainsKey("projectSorting"))
                        {
                            if (checkListArr[i] == 1)
                            {
                                data["projectSorting"] = "2";
                            }
                            else
                            {
                           //     data["projectSorting"] = "0";
                            }
                        }
                        else
                        {
                            if (checkListArr[i] == 1)
                            {
                                data.Add("projectSorting", "2");
                            }
                            else
                            {
                              //  data.Add("projectSorting", "0");
                            }
                        }
                        break;

                }
            }

            Console.WriteLine("Final Hash"+propFile);
            foreach (KeyValuePair<string, string> kvp in data)
            {
                Console.WriteLine(string.Format("{0} {1}", kvp.Key, kvp.Value));
            }


            //blank kar rhe hai 
            try
            {
                System.IO.File.WriteAllText(propFile, string.Empty);
            }
            catch (Exception e)
            {
                Console.WriteLine("clean" + e.Message);
            }


            //writing data in properties files
            foreach (KeyValuePair<string, string> kvp in data)
            {
                System.IO.File.AppendAllText(propFile, string.Format("{0}{1}{2}",kvp.Key,"="+kvp.Value,Environment.NewLine));
            }

            //string line;

            //// Read the file and display it line by line.  
            //System.IO.StreamReader file = new System.IO.StreamReader(propFile);
            //while ((line = file.ReadLine()) != null)
            //{
            //    System.Console.WriteLine("line"+line);
            //    line = line.Replace(" ", string.Empty);
            //    System.Console.WriteLine("line1" + line);
            //}

            //file.Close();

            done.Text = "Done";
        }
  

        private void readDic()
        {
            Console.WriteLine("fileName " + Path.GetFileNameWithoutExtension(filepath));
            propFile = Path.GetDirectoryName(filepath);

            propFile = Path.Combine(propFile, "WEB-INF");
            propFile = Path.Combine(propFile, "xml");
            propFile = Path.Combine(propFile, "sys_defaults.properties");
            Console.WriteLine("proFile", propFile);
            cleanPath(propFile);
            //creating backup directory
            propBackupPath = Path.Combine(propBackupPath, "BackUp");
            propBackupPath = Path.Combine(propBackupPath, "Properties" );
            createDirectory(propBackupPath);
            //copyinng backup file
            backUpFileProperties(propBackupPath);

            Console.WriteLine("readDic data reading file");
            //reading data from dic
            try
            {
                foreach (var row in File.ReadAllLines(propFile))
                    data.Add(row.Split('=')[0].Trim(), string.Join("=", row.Split('=').Skip(1).ToArray())); //trim function to get key without space
                    
                Console.WriteLine("readDic Print");              

                //trimimg string key value
                foreach (KeyValuePair<string, string> kvp in data)
                {
                    //textBox3.Text += ("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
                    Console.WriteLine("Key = {0}, Value = {1}",kvp.Key, kvp.Value);
                }
                
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            //removing emppty key in dictionary 
            data.Remove(" ");

        }


        private void createDirectory(string path)
        {
            bool folderExists = Directory.Exists(path);
          //  done.Text += path;
            Console.WriteLine("dir Path"+path+folderExists);
            if (!folderExists)
            {
                
                Directory.CreateDirectory(path);
                //MessageBox.Show(path+""+folderExists, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("folderExists" + folderExists);
            }
              
        }

        private void backUpFileProperties(string path)
        {
            try
            {
                string dateTime = DateTime.Now.ToString("ddMMyyy_hhmmss");// DateTime.Now.ToString(@"MM\:dd\:yyyy_h\:mm_tt");

                dateTime = dateTime + ".properties";
                string newName = Path.Combine(path, dateTime);
               // done.Text = "Prop"+newName;
                Console.WriteLine("xxx" + propFile + "yyy" + newName);

                File.Copy(propFile, newName,true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            //if (!Directory.Exists(path)) Directory.CreateDirectory(Path.Combine(newDirPath,dateTime));
        }

        private void backUpFileXml(string path)
        {
            try
            {
                string dateTime = DateTime.Now.ToString("ddMMyyy_hhmmss");// DateTime.Now.ToString(@"MM\:dd\:yyyy_h\:mm_tt");
                dateTime = dateTime + ".config";

                string newName = Path.Combine(path, dateTime);
                Console.WriteLine("newName"+newName);
                Console.WriteLine("filepathCopy"+filepath);
                Console.WriteLine("xxx" + propFile + "yyy" + newName);
                File.Copy(filepath, newName, true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            //if (!Directory.Exists(path)) Directory.CreateDirectory(Path.Combine(newDirPath,dateTime));
        }

        //write back functions for .config 


        private void requestFilteringOption(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("requestFiltering");

            /*
            listView1.Items[2].SubItems[0].Text = "No";
            listView1.Items[2].SubItems[2].Text = filepath;
            listView1.Items[2].SubItems[0].ForeColor = Color.Red;
            */

            for (int i = 0; i < elemList.Count; i++)
            {
               
                foreach (XmlNode no in elemList[i])
                {
                   

                    if (elemList[i].HasChildNodes == true)
                    {
                        foreach (XmlNode n in no)
                        {
                            Console.WriteLine(n.Name);


                            if (n.Attributes["verb"]?.InnerText == "OPTIONS")
                            {
                                Console.WriteLine("inside options");                   
                                n.Attributes["allowed"].Value = "true";
                               // doc.Save(filepath);
                                break;
                            }else
                            {
                                //need to think
                            }

                        }
                    }

                }
            }
            //   return 0;
            //at end try to do it once 

        }

        private void requestFilteringRules(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("requestFiltering");
            /*
              listView1.Items[1].SubItems[0].Text = "No";
              listView1.Items[1].SubItems[2].Text = filepath;
              listView1.Items[1].SubItems[0].ForeColor = Color.Red;
              */
            for (int i = 0; i < elemList.Count; i++)
            {

                foreach (XmlNode no in elemList[i])
                {                  
                    if (no.Name.ToString() == "verbs")
                    {

                        Console.WriteLine(no.Attributes["allowUnlisted"]?.InnerText);
                        if (no.Attributes["allowUnlisted"]?.InnerText == "false")
                        {

                            no.Attributes["allowUnlisted"].Value = "true";
                         //   doc.Save(filepath);
                          
                            break;
                        }
                        else
                        {

                        }
                    }

                }
            }
          
            //return 0;
        }

        private void timeOut(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("system.web");
            /*
            listView1.Items[7].SubItems[0].Text = "No";
            listView1.Items[7].SubItems[2].Text = filepath;
            listView1.Items[7].SubItems[0].ForeColor = Color.Red;
            */

            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("no.Name");
                    Console.WriteLine(no.Name);

                    if (no.Name.ToString() == "sessionState")
                    {                        
                        try
                        {

                           
                            no.Attributes["timeout"].Value = textBox1.Text;
                         //   doc.Save(filepath);
                            

                        }
                        catch(Exception e)
                        {
                            MessageBox.Show(e.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                       
                    
                        break;
                    }

                }
            }

        }
        private void enableVersionHeader(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("system.web");

          

            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("no.Name");
                    Console.WriteLine(no.Name);

                    if (no.Name.ToString() == "httpRuntime")
                    {
                        Console.WriteLine("inside httpRuntime");

                        Console.WriteLine(no.Attributes["enableVersionHeader"]?.InnerText);
                        if (no.Attributes["enableVersionHeader"]?.InnerText == "false") // ideal case
                        {

                            continue;

                        }else
                        {
                            no.Attributes["enableVersionHeader"].Value ="false";
                        }

                   //      doc.Save(filepath);
                        break;
                    }
                }
            }
           // return 0;
        }

        
        private void customError(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("system.web");
         

            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("no.Name");
                    Console.WriteLine(no.Name);
                    Label label1 = new Label();
                    if (no.Name.ToString() == "customErrors")
                    {
                        try
                        {

                            //no.Attributes["defaultRedirect"].Value = textBox2.Text.Trim();
                            XmlAttribute attr = doc.CreateAttribute("defaultRedirect");

                                attr.Value = textBox2.Text.Trim();
                                no.Attributes.SetNamedItem(attr); //setting attribute
                                no.Attributes["mode"].Value = "RemoteOnly";
                            
                        
                            //no.Attributes["mode"].Value = "RemoteOnly";
                          //  doc.Save(filepath);
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show(e.Message, "Warning" ,MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                       
                        break;
                    }
                }
            }
        }

     

        /// <summary>
        /// 
        /// </summary>
        /// <param name="doc"></param>
      

        private void nodeASPVersionNodes(XmlDocument doc)
        {

            Console.WriteLine("inside nodeASPVersionNodes");
            /*
            //string filterString = "<filteringRules><remove name = 'SQL_Injection_Mitigation'/><filteringRule name = 'SQL_Injection_Mitigation' scanUrl ='true' scanQueryString = 'true'><scanHeaders><clear><scanHeaders/><appliesTo><clear/<add fileExtension = '.asp'/><add fileExtension = '.aspx/><appliesTo/><denyStrings><add string = 'Select'/><add string = 'Drop'/><add string = 'Update'/><add string = 'Convert'/><add string = 'Alter'/><add string = 'Insert'/><add string = 'Count'/><add string = 'Delete'/><denyStrings/><filteringRule/><filteringRules/>";

            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");

            // filterString= HttpUtility.HtmlDecode(filterString);

            XmlNode node = elemList[0];


            XmlElement rewrite = doc.CreateElement("rewrite");
            //XmlTextReader reader = new XmlTextReader(filterString);

            //  string myEncodedString = System.Net.WebUtility.HtmlDecode(filterString);

            //string stringwanted = filterString.Replace("&lt;", "<")
            //                                       .Replace("&amp;", "&")
            //                                       .Replace("&gt;", ">")
            //                                       .Replace("&quot;", "\"")
            //                                       .Replace("&apos;", "'")
            //                                       .Replace("'", "\"");

            // "<remove name = 'SQL_Injection_Mitigation'/>"
            XmlElement outboundRules = doc.CreateElement("outboundRules");
           

            //<filteringRule name = 'SQL_Injection_Mitigation' scanUrl ='true' scanQueryString = 'true'>

            XmlElement rule = doc.CreateElement("rule");
            rule.SetAttribute("name", "RESPONSE_X-ASPNET-VERSION");

            

            XmlElement match = doc.CreateElement("match");
            match.SetAttribute("serverVariable", "RESPONSE_X-ASPNET-VERSION");
            match.SetAttribute("pattern", ".*");

            XmlElement action = doc.CreateElement("action");
            action.SetAttribute("type", "Rewrite");


            //   XmlDocument doc1 = new XmlDocument();
            // doc1.LoadXml(myEncodedString);

            // XElement newNode = XDocument.Parse(filterString).Root;
            // XmlText text = doc.CreateTextNode(stringwanted);
            //   objectRec.Value = stringwanted.ToString();
            // objectRec.InnerText = stringwanted;
            // Console.WriteLine("filterString"+objectRec);

            node.AppendChild(rewrite).AppendChild(outboundRules).AppendChild(rule).AppendChild(match);

            XmlNode rule11 = node.LastChild.LastChild.LastChild.LastChild;
            Console.WriteLine("rule11" + rule11);
            rule11.AppendChild(action);
              // node.InsertAfter(action, node.LastChild);

             //   node.InsertBefore(action,match) ;

            //XmlNode ruleNode = node;
            // xmldoc.DocumentElement.InsertBefore(newNode, xmlDoc.DocumentElement.LastChild);
            //node.InsertBefore(action, );
            //node.AppendChild("/ddd");
            
            doc.Save(filepath);
            */

            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
            XmlNode node = elemList[0];

            XmlElement rewrite = doc.CreateElement("rewrite");

            XmlElement outboundRules = doc.CreateElement("outboundRules");

            XmlElement rule = doc.CreateElement("rule");
            rule.SetAttribute("name", "RESPONSE_X-ASPNET-VERSION");

            XmlElement match = doc.CreateElement("match");
            match.SetAttribute("serverVariable", "RESPONSE_X-ASPNET-VERSION");
            match.SetAttribute("pattern", ".*");

            XmlElement action = doc.CreateElement("action");
            action.SetAttribute("type", "Rewrite");

            if (doc.SelectSingleNode("//system.webServer/rewrite/outboundRules") == null)
            {
                //Console.WriteLine("inside nodeASPVersionNodes outbounds dont exists");
                //if checking 
                if (doc.SelectSingleNode("//system.webServer/rewrite") == null)
                {
                    Console.WriteLine("inside nodeASPVersionNodes rewrite dont exists");
                    //if for rewrite    
                    node.AppendChild(rewrite).AppendChild(outboundRules).AppendChild(rule);
                    rule.AppendChild(match);
                    rule.AppendChild(action);

                    //XmlNode addAction = node.LastChild.LastChild.LastChild;
                    //Console.WriteLine("rule11" + addAction);
                    //addAction.AppendChild(action);
                }
                else
                {
                    Console.WriteLine("inside nodeASPVersionNodes rewrite exists");
                    //adding match
                    node.AppendChild(outboundRules).AppendChild(rule);
                    rule.AppendChild(match);
                    rule.AppendChild(action);
                    //XmlNode addAction = node.LastChild.LastChild.LastChild.LastChild.LastChild;
                    //Console.WriteLine("rule11" + addAction);
                    //addAction.AppendChild(action);
                }

            }
            else
            {
                Console.WriteLine("yes path exists nodeASPVersionNodes");

            //    writeHidingAspVersion(doc);              
             
            }
      //      doc.Save(filepath);
            /*            if (doc.SelectNodes("//system.webServer/rewrite/outboundRules/rule/match") == null)
            {
                Console.WriteLine("yes path exists");
                //writehidingServerResponse(doc);
                writeHidingAspVersion(doc);
            }
            else
            {
                if (doc.SelectNodes("//system.webServer/rewrite") == null)
                {
                    node.AppendChild(rewrite);
                }
                else
                {
                    //rewrite node exists
                    if (doc.SelectNodes("//system.webServer/rewrite/outboundRules") == null)
                    {
                        node.AppendChild(outboundRules);
                    }
                    else
                    {
                        //outbounRules node exists
                        node.AppendChild(match);

                        XmlNode addAction = node.LastChild.LastChild.LastChild.LastChild;
                        Console.WriteLine("rule11" + addAction);
                        addAction.AppendChild(action);
                    }
                }
            }

            //node.LastChild.AppendChild(rewrite).AppendChild(outboundRules).AppendChild(rule).AppendChild(match);    
            doc.Save(filepath);
            */
        }

        private void nodeHidingServerResponse(XmlDocument doc)
        {
            Console.WriteLine("inside nodeHidingServerResponse");
            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
            XmlNode node = elemList[0];

            XmlElement rewrite = doc.CreateElement("rewrite");

            XmlElement outboundRules = doc.CreateElement("outboundRules");

            XmlElement rule = doc.CreateElement("rule");
            rule.SetAttribute("name", "RESPONSE_SERVER");

            XmlElement match = doc.CreateElement("match");
            match.SetAttribute("serverVariable", "RESPONSE_SERVER");
            match.SetAttribute("pattern", ".*");

            XmlElement action = doc.CreateElement("action");
            action.SetAttribute("type", "Rewrite");

            if (doc.SelectSingleNode("//system.webServer/rewrite/outboundRules") == null)
            {
                //Console.WriteLine("inside nodeASPVersionNodes outbounds dont exists");
                //if checking 
                if (doc.SelectSingleNode("//system.webServer/rewrite") == null)
                {
                    Console.WriteLine("inside nodeHidingServerResponse rewrite dont exists");
                    //if for rewrite    
                    node.AppendChild(rewrite).AppendChild(outboundRules).AppendChild(rule).AppendChild(match);
                    XmlNode addAction = node.LastChild.LastChild.LastChild;
                    //Console.WriteLine("rule11" + addAction);
                    addAction.AppendChild(action);
                }
                else
                {
                    Console.WriteLine("inside nodeHidingServerResponse rewrite exists");
                    //adding match
                    node.AppendChild(outboundRules).AppendChild(rule).AppendChild(match);
                    XmlNode addAction = node.LastChild.LastChild.LastChild.LastChild.LastChild;
                    //Console.WriteLine("rule11" + addAction);
                    addAction.AppendChild(action);

                }

            }
            else
            {
                XmlNode addAction2 = node.LastChild.LastChild;
                addAction2.AppendChild(rule);

                //isme rule banana padega pura
                XmlNode addAction1 = node.LastChild.LastChild.LastChild;
                addAction1.AppendChild(match);

                    //node.AppendChild(match);

                XmlNode addAction = node.LastChild.LastChild.LastChild;
                addAction.AppendChild(action);

               // writehidingServerResponse(doc);
               
            }

            //node.LastChild.AppendChild(rewrite).AppendChild(outboundRules).AppendChild(rule).AppendChild(match);    
         //   doc.Save(filepath);
      }

        private void nodehidingServerResponse(XmlDocument doc)
        {
            Console.WriteLine("inside nodeHidingServerResponse");
            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
            XmlNode node = elemList[0];

            XmlElement rewrite = doc.CreateElement("rewrite");

            XmlElement outboundRules = doc.CreateElement("outboundRules");

            XmlElement rule = doc.CreateElement("rule");
            rule.SetAttribute("name", "RESPONSE_X-POWERED-BY");

            XmlElement match = doc.CreateElement("match");
            match.SetAttribute("serverVariable", "RESPONSE_X-POWERED-BY");
            match.SetAttribute("pattern", ".*");

            XmlElement action = doc.CreateElement("action");
            action.SetAttribute("type", "Rewrite");

            if (doc.SelectSingleNode("//system.webServer/rewrite/outboundRules") == null)
            {
                //Console.WriteLine("inside nodeASPVersionNodes outbounds dont exists");
                //if checking 
                if (doc.SelectSingleNode("//system.webServer/rewrite") == null)
                {
                    Console.WriteLine("inside nodeHidingServerResponse rewrite dont exists");
                    //if for rewrite    
                    node.AppendChild(rewrite).AppendChild(outboundRules).AppendChild(rule).AppendChild(match);


                    XmlNode addAction = node.LastChild.LastChild.LastChild;
                    //Console.WriteLine("rule11" + addAction);
                    addAction.AppendChild(action);
                }
                else
                {
                    Console.WriteLine("inside nodeHidingServerResponse rewrite exists");
                    //adding match
                    node.AppendChild(outboundRules).AppendChild(rule).AppendChild(match);
                    XmlNode addAction = node.LastChild.LastChild.LastChild.LastChild.LastChild;
                    //Console.WriteLine("rule11" + addAction);
                    addAction.AppendChild(action);

                }

            }
            else
            {
                XmlNode addAction2 = node.LastChild.LastChild;
                addAction2.AppendChild(rule);

                //isme rule banana padega pura
                XmlNode addAction1 = node.LastChild.LastChild.LastChild;
                addAction1.AppendChild(match);

                //node.AppendChild(match);

                XmlNode addAction = node.LastChild.LastChild.LastChild;
                addAction.AppendChild(action);

             

            }

            //node.LastChild.AppendChild(rewrite).AppendChild(outboundRules).AppendChild(rule).AppendChild(match);    
          //  doc.Save(filepath);
        }


      


        private void nodeHTTPCookie(XmlDocument doc)
        {

            //< rule name = "Add HttpOnly" preCondition = "No HttpOnly" >
            //         < match serverVariable = "RESPONSE_Set_Cookie" pattern = ".*" negate = "false" />    
            //            < action type = "Rewrite" value = "{R:0}; HttpOnly" />
            //             < conditions >
            //             </ conditions >
            //         </ rule >
            //         < preConditions >
            //             < preCondition name = "No HttpOnly" >
            //                  < add input = "{RESPONSE_Set_Cookie}" pattern = "." />
            //                    < add input = "{RESPONSE_Set_Cookie}" pattern = "; HttpOnly" negate = "true" />
            //                   </ preCondition >
            //               </ preConditions >


            Console.WriteLine("inside nodeHidingServerResponse");
            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
            XmlNode node = elemList[0];

            XmlElement rewrite = doc.CreateElement("rewrite");

            XmlElement outboundRules = doc.CreateElement("outboundRules");

            //First Rule


            XmlElement rule = doc.CreateElement("rule");
            rule.SetAttribute("name", "RESPONSE_Strict_Transport_Security");
            rule.SetAttribute("enabled", "true");

            XmlElement match = doc.CreateElement("match");
            match.SetAttribute("serverVariable", "RESPONSE_Set_Cookie");
            match.SetAttribute("pattern", ".*");

            XmlElement conditions = doc.CreateElement("conditions");

            XmlElement add = doc.CreateElement("add");
            add.SetAttribute("input", "{HTTPS}");
            add.SetAttribute("pattern", "on");

            XmlElement action = doc.CreateElement("action");
            action.SetAttribute("type", "Rewrite");
            action.SetAttribute("value", "max-age=31536000");


            rule.AppendChild(match);
            rule.AppendChild(conditions).AppendChild(add);
            rule.AppendChild(action);

            //second rule

            XmlElement rule1 = doc.CreateElement("rule");
            rule1.SetAttribute("name", "Add HttpOnly");
            rule1.SetAttribute("preCondition", "No HttpOnly");



            XmlElement match1 = doc.CreateElement("match");
            match1.SetAttribute("serverVariable", "RESPONSE_Set_Cookie");
            match1.SetAttribute("pattern", ".*");
            match1.SetAttribute("negate", "false");

            XmlElement action1 = doc.CreateElement("action");
            action1.SetAttribute("type", "Rewrite");
            action1.SetAttribute("value", "{R:0}; HttpOnly");

            XmlElement conditions1 = doc.CreateElement("conditions");


            rule1.AppendChild(match1);
            rule1.AppendChild(action1);
            rule1.AppendChild(conditions1);

            //preConditions
            XmlElement preConditions = doc.CreateElement("preConditions");

            XmlElement preCondition = doc.CreateElement("preCondition");
            preCondition.SetAttribute("name", "No HttpOnly");


            XmlElement add3 = doc.CreateElement("add");
            add3.SetAttribute("input", "{RESPONSE_Set_Cookie}");
            add3.SetAttribute("pattern", ".*");


            XmlElement add4 = doc.CreateElement("add");
            add4.SetAttribute("input", "{RESPONSE_Set_Cookie}");
            add4.SetAttribute("pattern", ".*");
            add4.SetAttribute("negate", "true");

            preCondition.AppendChild(add3);
            preCondition.AppendChild(add4);
            preConditions.AppendChild(preCondition);


            if (doc.SelectSingleNode("//system.webServer/rewrite/outboundRules") == null)
            {
                //Console.WriteLine("inside nodeASPVersionNodes outbounds dont exists");
                //if checking 
                if (doc.SelectSingleNode("//system.webServer/rewrite") == null)
                {
                    Console.WriteLine("inside nodeHidingServerResponse rewrite dont exists");
                    //if for rewrite    
                    outboundRules.AppendChild(rule);
                    outboundRules.AppendChild(rule1);
                    outboundRules.AppendChild(preConditions);
                    node.AppendChild(rewrite).AppendChild(outboundRules); //.AppendChild(rule); //.AppendChild(match);

                    //   XmlNode addAction = node.LastChild.LastChild.LastChild;
                    //Console.WriteLine("rule11" + addAction);
                    //  addAction.AppendChild(action1);
                }
                else
                {
                    Console.WriteLine("inside nodeHidingServerResponse rewrite exists");
                    //adding match
                    outboundRules.AppendChild(rule);
                    outboundRules.AppendChild(rule1);
                    outboundRules.AppendChild(preConditions);
                    node.AppendChild(outboundRules);//.AppendChild(rule).AppendChild(match);

                    // XmlNode addAction = node.LastChild.LastChild.LastChild.LastChild.LastChild;
                    //Console.WriteLine("rule11" + addAction);
                    //addAction.AppendChild(action1);

                }

            }
            else
            {
                outboundRules.AppendChild(rule);
                outboundRules.AppendChild(rule1);
                outboundRules.AppendChild(preConditions);
                node.AppendChild(rewrite).AppendChild(outboundRules);
                /*
                XmlNode addAction2 = node.LastChild.LastChild;
                addAction2.AppendChild(rule1);

                //isme rule banana padega pura
                XmlNode addAction1 = node.LastChild.LastChild.LastChild;
                addAction1.AppendChild(match1);

                //node.AppendChild(match);

                XmlNode addAction = node.LastChild.LastChild.LastChild;
                addAction.AppendChild(action1);

                writehidingServerResponse(doc);
                */

            }
          //  doc.Save(filepath);
        }
        //node.LastChild.AppendChild(rewrite).AppendChild(outboundRules).AppendChild(rule).AppendChild(match);    


        private void writeHstsheader(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");

            // < add name = "Strict-Transport-Security" value = "max-age=31536000" />
            XmlElement add = doc.CreateElement("add");
            add.SetAttribute("name", "Strict-Transport-Security");
            add.SetAttribute("value", "max-age=31536000");

            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("hstsheadertags" + no.Name);
                    if (no.Name.ToString() == "httpProtocol")
                    {
                    
                        foreach (XmlNode n in no)
                        {
                            Console.WriteLine(n.Name);
                            if (n.Name == "customHeaders")
                            {
                                Console.WriteLine("try1" + n.Name);
                                foreach (XmlNode m in n)
                                {
                                    Console.WriteLine("try2" + m.Name);
                                    if (m.Name == "add")
                                    {

                                        if (m.Attributes["name"]?.InnerText == "Strict-Transport-Security" && m.Attributes["value"]?.InnerText == "max-age=31536000")
                                        {
                                          //  Console.WriteLine("try3" + m.Attributes["name"]?);
                                            Console.WriteLine(m.Attributes["name"]?.InnerText);
                                            //     settingsListBox.SetSelected(6, true);
                                            //    settingsListBox.SetItemChecked(6, true);

                                        }
                                        else
                                        {   
                                            Console.WriteLine("time to add ");

                                            //listView1.Items[listView1.SelectedIndices[6]].Selected = false;

                                            n.AppendChild(add);
                                            //
                                            
                                            // item.Selected = true; // false in case of unselect
                                            //item.Checked = true;
                                            //

                                            
                                        //    doc.Save(filepath);                             
                                            //m.Attributes["name"].Value = "Strict-Transport-Security";
                                            //m.Attributes["value"].Value = "max-age=31536000";
                                        }


                                    }

                                }

                            }
                        }
                    }
                }


            }
          
        }

        private void outBounds(XmlDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
            XmlNode node = elemList[0];

            XmlElement outboundRules = doc.CreateElement("outboundRules");
            XmlElement rewrite = doc.CreateElement("rewrite");

            if (doc.SelectSingleNode("//system.webServer/rewrite/outboundRules") == null)
            {
                Console.WriteLine("inside outbounds dont exists");
                //if checking 
                if (doc.SelectSingleNode("//system.webServer/rewrite") == null)
                {
                    Console.WriteLine("inside rewrite dont exists");
                    node.AppendChild(rewrite).AppendChild(outboundRules);
                }
                else
                {
                    doc.SelectSingleNode("//system.webServer/rewrite").AppendChild(outboundRules);
                }

            }
          //  doc.Save(filepath);
        }

        private void ASPVersionNode(XmlDocument doc)
        {
            Console.WriteLine("inside nodeASPVersionNodes");
           
            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
            XmlNode node = elemList[0];

            XmlElement rule = doc.CreateElement("rule");
            rule.SetAttribute("name", "RESPONSE_X-ASPNET-VERSION");

            XmlElement match = doc.CreateElement("match");
            match.SetAttribute("serverVariable", "RESPONSE_X-ASPNET-VERSION");
            match.SetAttribute("pattern", ".*");

            XmlElement action = doc.CreateElement("action");
            action.SetAttribute("type", "Rewrite");

            rule.AppendChild(match);
            rule.AppendChild(action);

            if (doc.SelectSingleNode("//system.webServer/rewrite/outboundRules/rule") == null)
            {
                doc.SelectSingleNode("//system.webServer/rewrite/outboundRules").AppendChild(rule);
            }else
            {
                if (doc.SelectSingleNode("//system.webServer/rewrite/outboundRules/rule").Attributes["name"]?.InnerText == "RESPONSE_X-ASPNET-VERSION")
                {

                }
                else
                {
                    doc.SelectSingleNode("//system.webServer/rewrite/outboundRules").AppendChild(rule);
                }
            }
           //     doc.Save(filepath);
          
        }
        private void HTTPCookie(XmlDocument doc)
        {
            Console.WriteLine("inside HTTPCookie");

            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
            XmlNode node = elemList[0];

            XmlElement rewrite = doc.CreateElement("rewrite");

            XmlElement outboundRules = doc.CreateElement("outboundRules");

            //First Rule


            XmlElement rule = doc.CreateElement("rule");
            rule.SetAttribute("name", "RESPONSE_Strict_Transport_Security");
            rule.SetAttribute("enabled", "true");

            XmlElement match = doc.CreateElement("match");
            match.SetAttribute("serverVariable", "RESPONSE_Set_Cookie");
            match.SetAttribute("pattern", ".*");

            XmlElement conditions = doc.CreateElement("conditions");

            XmlElement add = doc.CreateElement("add");
            add.SetAttribute("input", "{HTTPS}");
            add.SetAttribute("pattern", "on");

            XmlElement action = doc.CreateElement("action");
            action.SetAttribute("type", "Rewrite");
            action.SetAttribute("value", "max-age=31536000");


            rule.AppendChild(match);
            rule.AppendChild(conditions).AppendChild(add);
            rule.AppendChild(action);

            //second rule

            XmlElement rule1 = doc.CreateElement("rule");
            rule1.SetAttribute("name", "Add HttpOnly");
            rule1.SetAttribute("preCondition", "No HttpOnly");



            XmlElement match1 = doc.CreateElement("match");
            match1.SetAttribute("serverVariable", "RESPONSE_Set_Cookie");
            match1.SetAttribute("pattern", ".*");
            match1.SetAttribute("negate", "false");

            XmlElement action1 = doc.CreateElement("action");
            action1.SetAttribute("type", "Rewrite");
            action1.SetAttribute("value", "{R:0}; HttpOnly");

            XmlElement conditions1 = doc.CreateElement("conditions");


            rule1.AppendChild(match1);
            rule1.AppendChild(action1);
            rule1.AppendChild(conditions1);

            //preConditions
            XmlElement preConditions = doc.CreateElement("preConditions");

            XmlElement preCondition = doc.CreateElement("preCondition");
            preCondition.SetAttribute("name", "No HttpOnly");


            XmlElement add3 = doc.CreateElement("add");
            add3.SetAttribute("input", "{RESPONSE_Set_Cookie}");
            add3.SetAttribute("pattern", ".*");


            XmlElement add4 = doc.CreateElement("add");
            add4.SetAttribute("input", "{RESPONSE_Set_Cookie}");
            add4.SetAttribute("pattern", ".*");
            add4.SetAttribute("negate", "true");

            preCondition.AppendChild(add3);
            preCondition.AppendChild(add4);
            preConditions.AppendChild(preCondition);

            if (doc.SelectSingleNode("//system.webServer/rewrite/outboundRules/rule") == null)
            {
                doc.SelectSingleNode("//system.webServer/rewrite/outboundRules").AppendChild(rule);
                doc.SelectSingleNode("//system.webServer/rewrite/outboundRules").AppendChild(rule1);
                doc.SelectSingleNode("//system.webServer/rewrite/outboundRules").AppendChild(preConditions);
            }
            else
            {
                if (doc.SelectSingleNode("//system.webServer/rewrite/outboundRules/rule").Attributes["name"]?.InnerText.Trim() == "Add Strict-Transport-Security when HTTPS" || doc.SelectSingleNode("//system.webServer/rewrite/outboundRules/rule").Attributes["name"]?.InnerText.Trim() == "Add HttpOnly")
                {

                }
                else
                {   
                    doc.SelectSingleNode("//system.webServer/rewrite/outboundRules").AppendChild(rule);
                    doc.SelectSingleNode("//system.webServer/rewrite/outboundRules").AppendChild(rule1);
                    doc.SelectSingleNode("//system.webServer/rewrite/outboundRules").AppendChild(preConditions);
                  
                }
            }

          //  doc.Save(filepath);
        }

        private void HidingServer(XmlDocument doc)
        {
            Console.WriteLine("inside HidingServer");

            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
            XmlNode node = elemList[0];

            XmlElement rule = doc.CreateElement("rule");
            rule.SetAttribute("name", "RESPONSE_SERVER");

            XmlElement match = doc.CreateElement("match");
            match.SetAttribute("serverVariable", "RESPONSE_SERVER");
            match.SetAttribute("pattern", ".*");

            XmlElement action = doc.CreateElement("action");
            action.SetAttribute("type", "Rewrite");

            rule.AppendChild(match);
            rule.AppendChild(action);

            if (doc.SelectSingleNode("//system.webServer/rewrite/outboundRules/rule") == null)
            {
                doc.SelectSingleNode("//system.webServer/rewrite/outboundRules").AppendChild(rule);
            }
            else
            {
                if (doc.SelectSingleNode("//system.webServer/rewrite/outboundRules/rule").Attributes["name"]?.InnerText == "RESPONSE_SERVER")
                {
                    Console.WriteLine("second if");
                }
                else
                {
                    Console.WriteLine("second else");
                    doc.SelectSingleNode("//system.webServer/rewrite/outboundRules").AppendChild(rule);
                }
            }
       //     doc.Save(filepath);
        }

        private void HidingServerResponse(XmlDocument doc)
        {
            Console.WriteLine("inside HidingServerResponse");

            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
            XmlNode node = elemList[0];

            XmlElement rule = doc.CreateElement("rule");
            rule.SetAttribute("name", "RESPONSE_X-POWERED-BY");

            XmlElement match = doc.CreateElement("match");
            match.SetAttribute("serverVariable", "RESPONSE_X-POWERED-BY");
            match.SetAttribute("pattern", ".*");

            XmlElement action = doc.CreateElement("action");
            action.SetAttribute("type", "Rewrite");

            rule.AppendChild(match);
            rule.AppendChild(action);

            if (doc.SelectSingleNode("//system.webServer/rewrite/outboundRules/rule") == null)
            {
                doc.SelectSingleNode("//system.webServer/rewrite/outboundRules").AppendChild(rule);
            }
            else
            {
                if (doc.SelectSingleNode("//system.webServer/rewrite/outboundRules/rule").Attributes["name"]?.InnerText == "RESPONSE_X-POWERED-BY")
                {

                }
                else
                {
                    doc.SelectSingleNode("//system.webServer/rewrite/outboundRules").AppendChild(rule);
                }
            }
          //  doc.Save(filepath);
        }

        private void SelectAll_Click(object sender, EventArgs e)
        {
            ListView.CheckedListViewItemCollection checkedItems = listView1.CheckedItems;

            if (flag == 0)
            {
                flag = 1;
            }else
            {
                flag = 0;
            }

            if (flag == 1)
            {
                foreach (ListViewItem item in listView1.Items)
                {
                    item.Selected = true; // false in case of unselect
                    item.Checked = true;
                    SelectAll.Text = "DeSelectAll";
                }
            }
            else
            {
                foreach (ListViewItem item in listView1.Items)
                {
                    item.Selected =false  ; // false in case of unselect
                    item.Checked = false;
                    SelectAll.Text = "SelectAll";
                }

            }
        }

        private void cleanPath(string path)
        {
            Console.WriteLine("path"+path);
            File.WriteAllLines(path, File.ReadAllLines(path).Where(l => !string.IsNullOrWhiteSpace(l)));
            //string.Join(" ", File.ReadAllText(path, Encoding.Default).Split(new string[] { " ", "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries));
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }



        // ##params checking 

        private void CheckhttpCookie(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");

            Console.WriteLine("inside httpCookie");
            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {
                  
                    Console.WriteLine(no.Name);

                    if (no.Name.ToString() == "rewrite")
                    {
                        foreach (XmlNode n in no)
                        {
                            Console.WriteLine(n.Name);
                            if (n.Name == "outboundRules")
                            {
                                Console.WriteLine("inside outboundRules");
                                
                                if (n.ChildNodes.Count == 0)
                                {
                                    Console.WriteLine("null");
                                    HTTPCookie(doc);
                                    
                                }
                                int counter = 0;
                                foreach (XmlNode m in n)
                                {
                                    Console.WriteLine(m.Name);
                                    if (m.Name == "rule")
                                    {
                                        Console.WriteLine("inside rule");
                                        foreach (XmlNode o in m)
                                        {
                                            if (o.Name == "match")
                                            {
                                                Console.WriteLine("match");

                                               Console.WriteLine(o.Name);
                                                if (o.Attributes["serverVariable"]?.InnerText == "RESPONSE_Set_Cookie")//&& o.Attributes["input"]?.InnerText == "{RESPONSE_Set_Cookie}")
                                                {
                                                    Console.WriteLine("httpcookie exists");
                                                    counter += 1;
                                                    break;
                                                   
                                                }                                             

                                            }
                                        }
                                       
                                    }

                                }
                                if (counter  == 0)
                                {
                                    HTTPCookie(doc);
                                }
                            }


                        }
                    }

                }
            }
        }

        private void CheckhidingServer(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("no.Name");
                    Console.WriteLine(no.Name);

                    if (no.Name.ToString() == "rewrite")
                    {
                        Console.WriteLine("inside rewrite");

                        foreach (XmlNode n in no)
                        {

                            if (n.Name.ToString() == "outboundRules")
                            {
                                Console.WriteLine("inside outboundRules");


                                if (n.ChildNodes.Count == 0)
                                {
                                    HidingServer(doc);
                                    Console.WriteLine("null");
                                }
                                int counter = 0;                   
                                foreach (XmlNode m in n)
                                {
                                    if (m.Name.ToString() == "rule")
                                    {
                                        Console.WriteLine(m.Name);
                                        if (m.Attributes["name"]?.InnerText == "RESPONSE_SERVER")
                                        {
                                            counter += 1; 
                                            Console.WriteLine("response_server exists");
                                            break;
                                        }
                                    }
                                }
                                if (counter == 0)
                                {
                                    HidingServer(doc);
                                }
                            }
                        }
              
                    }

                }
            }
        
        }

        //hsts header checking uses 2 functions

        private int Checkhstsheader1(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
            /*
            listView1.Items[9].SubItems[0].Text = "No";
            listView1.Items[9].SubItems[2].Text = filepath;
            listView1.Items[9].SubItems[0].ForeColor = Color.Red;
            */
            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("no.Name");
                    Console.WriteLine(no.Name);

                    if (no.Name.ToString() == "rewrite")
                    {
                        Console.WriteLine("inside rewrite");

                        foreach (XmlNode n in no)
                        {

                            if (n.Name.ToString() == "outboundRules")
                            {

                                if (n.ChildNodes.Count == 0)
                                {
                                    return 0;
                                 //   HTTPCookie(doc);
                                  //  Console.WriteLine("null");
                                }
                               

                                foreach (XmlNode m in n)
                                {
                                    if (m.Name.ToString() == "rule")
                                    {
                                        Console.WriteLine(m.Name);
                                        if (m.Attributes["name"]?.InnerText == "Add Strict-Transport-Security when HTTPS")
                                        {

                                            foreach (XmlNode o in m)
                                            {
                                                if (o.Name.ToString() == "match")
                                                {
                                                    //Console.WriteLine("inside match111");
                                                    ;
                                                    if (o.Attributes["serverVariable"]?.InnerText == "RESPONSE_Strict_Transport_Security")
                                                    {
                                                        Console.WriteLine("serverVariable" + m.Attributes["serverVariable"]?.InnerText);
                                                        return 1;
                                                    }
                                                }

                                            }

                                        }

                                    }
                                }
                            }
                        }

                    }

                }
            }
            return 0;
        }

        private int Checkhstsheader(XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
            /*
            Console.WriteLine("hstsheader");
            listView1.Items[6].SubItems[0].Text = "No";
            listView1.Items[6].SubItems[2].Text = filepath;
            listView1.Items[6].SubItems[0].ForeColor = Color.Red;
            */

            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("hstsheadertags" + no.Name);
                    if (no.Name.ToString() == "httpProtocol")
                    {
                        foreach (XmlNode n in no)
                        {
                            Console.WriteLine(n.Name);
                            if (n.Name == "customHeaders")
                            {
                                Console.WriteLine("try1" + n.Name);
                                foreach (XmlNode m in n)
                                {
                                    Console.WriteLine("try2" + m.Name);
                                    if (m.Name == "add")
                                    {

                                        if (m.Attributes["name"]?.InnerText == "Add Strict-Transport-Security when HTTPS" || m.Attributes["name"]?.InnerText == "Strict-Transport-Security")//&& m.Attributes["value"]?.InnerText == "max-age=31536000")
                                        {
                                            return 1;
                                            

                                        }


                                    }

                                }

                            }
                        }
                    }
                }


            }


            return 0;
        }

        private void CheckhidingAspVersion (XmlDataDocument doc)
        {
            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");
            /*
            listView1.Items[9].SubItems[0].Text = "No";
            listView1.Items[9].SubItems[2].Text = filepath;
            listView1.Items[9].SubItems[0].ForeColor = Color.Red;
            */
            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("no.Name");
                    Console.WriteLine(no.Name);

                    if (no.Name.ToString() == "rewrite")
                    {
                        Console.WriteLine("inside rewrite");

                        foreach (XmlNode n in no)
                        {

                            if (n.Name.ToString() == "outboundRules")
                            {


                                Console.WriteLine("inside outboundRules");


                                if (n.ChildNodes.Count == 0)
                                {
                                    ASPVersionNode(doc);
                                    Console.WriteLine("null");
                                }

                                int counter = 0;
                                foreach (XmlNode m in n)
                                {
                                    if (m.Name.ToString() == "rule")
                                    {
                                        Console.WriteLine(m.Name);
                                        if (m.Attributes["name"]?.InnerText == "RESPONSE_X-ASPNET-VERSION")
                                        {

                                            foreach (XmlNode o in m)
                                            {
                                                if (o.Name.ToString() == "match")
                                                {
                                                    Console.WriteLine("inside match111");
                                                    Console.WriteLine("serverVariable" + o.Attributes["serverVariable"]?.InnerText);
                                                    if (o.Attributes["serverVariable"]?.InnerText == "RESPONSE_X-ASPNET-VERSION" && o.Attributes["pattern"]?.InnerText == ".*")
                                                    {
                                                        Console.WriteLine("serverVariable" + m.Attributes["serverVariable"]?.InnerText);
                                                        counter += 1;
                                                        break;
                                                    }
                                                }

                                            }

                                        }

                                    }
                                }
                                if (counter == 0)
                                {
                                    ASPVersionNode(doc);
                                }
                            }
                        }                  
                    }

                }
            }
        }


        private void CheckinghidingServerResponse(XmlDataDocument doc)
        {
            Console.WriteLine("CheckinghidingServerResponse");
            XmlNodeList elemList = doc.GetElementsByTagName("system.webServer");

            for (int i = 0; i < elemList.Count; i++)
            {
                foreach (XmlNode no in elemList[i])
                {
                    Console.WriteLine("no.Name");
                    Console.WriteLine(no.Name);

                    if (no.Name.ToString() == "rewrite")
                    {
                        Console.WriteLine("inside rewrite");

                        foreach (XmlNode n in no)
                        {

                            if (n.Name.ToString() == "outboundRules")
                            {
                                Console.WriteLine("inside outboundRules");


                                if( n.ChildNodes.Count == 0)
                                {
                                    HidingServerResponse(doc);
                                    Console.WriteLine("null");
                                }

                                int counter = 0;
                                foreach (XmlNode m in n)
                                {
                                    if (m.Name.ToString() == "rule")
                                    {
                                        Console.WriteLine(m.Name);
                                        if (m.Attributes["name"]?.InnerText == "RESPONSE_X-POWERED-BY")
                                        {

                                            foreach (XmlNode o in m)
                                            {
                                                if (o.Name.ToString() == "match")
                                                {
                                                    //Console.WriteLine("inside match111");
                                                    
                                                    if (o.Attributes["serverVariable"]?.InnerText == "RESPONSE_X-POWERED-BY" && o.Attributes["pattern"]?.InnerText == ".*")
                                                    {
                                                        Console.WriteLine("serverVariable" + m.Attributes["serverVariable"]?.InnerText);
                                                        counter += 1;
                                                        break;
                                                    }
                                                }

                                            }

                                        }

                                    }
                                    
                                }
                                if (counter == 0)
                                {
                                  
                                    HidingServerResponse(doc);
                                }
                            }
                        }

                    }

                }
            }
          

        }

        private void button2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                inetPath = folderBrowserDialog1.SelectedPath;

            }

            if (inetPath != null)
            {
                inetPath = Path.Combine(inetPath, "wwwroot");
                inetPath = Path.Combine(inetPath, "web.config");
                inetlabel.Text = inetPath;
            }
            else
            {
                MessageBox.Show("Please select filepath", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }

}
