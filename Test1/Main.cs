﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test1
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }
      ///  Main main = new Main();
        private void button1_Click(object sender, EventArgs e)
        {
            ConfigurationForm configForm = new ConfigurationForm();

          //  this.Hide();

           // configForm.Closed += (s, args) => this.Close();
            //  form2.Show();
            // Show the settings form
            //configForm.ShowDialog();

            //Form2 frm2 = new Form2();
            configForm.Activated += new EventHandler(configForm_Activated);

            configForm.FormClosed += new FormClosedEventHandler(configForm_FormClosed);

            configForm.ShowDialog(this);

        }

     

        private void configForm_Activated(object sender, EventArgs e)
        {
            this.Hide(); // Hides Form1 but it is till in Memory
        }

        private void configForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            ConfigurationForm configForm = new ConfigurationForm();
            configForm.Close();
            this.Show();
            //this.Close(); // Closes Form1 and remove this time from Memory

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 settingsForm = new Form1();

            // this.Hide();

            //settingsForm.Closed += (s, args) => this.Close();
            //  form2.Show();
            // Show the settings form
            settingsForm.Activated += new EventHandler(settingsForm_Activated);
            settingsForm.FormClosed += new FormClosedEventHandler(settingsForm_FormClosed);
            settingsForm.ShowDialog(this);

        }



        private void settingsForm_Activated(object sender, EventArgs e)
        {
            this.Hide(); // Hides Form1 but it is till in Memory
        }

        private void settingsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form1 settingsForm = new Form1();
            settingsForm.Close();

            Main main = new Main();
            main.Show();

          //  Main main = new Main();
            //this.Show();
            //this.Close(); // Closes Form1 and remove this time from Memory

        }
    }

}
